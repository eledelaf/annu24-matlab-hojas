%% Practicas de Matlab
%% Metodos adaptativos
%% Hoja 7
% *Nombre:* Elena
% 
% *Apellido:* de la Fuente
% 
% *DNI:* 23821952R
% 
% *Email:* eledelaf@ucm.es
%% 
% %% *Par encajado*
% $h_{op}$ está dado por 
% 
% $$      h_{opt}=\mbox{mín}      \left(        \mbox{{ HMAX}},h_{n}\mbox{mín}\left(\mbox{{FACMAX,FAC}}          
% \left(            \frac{TOL    h_{n}}{\mathrm{Error}}          \right)^{\frac{1}{p+1}}        
% \right)      \right)$$
% 
% _FACMAX_ se suele tomar entre $1.5$ y $5$. 
% 
% %% 
%% Práctica 1: (Runge-Kutta Fehlberg-4-5) 
% La tabla de RKF-45 está dada por
% 
% $$    \begin{array}{c|cccccc}      0 &  & & & & & \\[0.2cm]      \frac{1}{4}& 
% \frac{1}{4} & & & & & \\[0.2cm]      \frac{3}{8}& \frac{3}{32}& \frac{9}{32} 
% &  & & &\\[0.2cm]      \frac{12}{13} & \frac{1932}{2197} & -\frac{7200}{2197} 
% &      \frac{7296}{2197} & & & \\[0.2cm]      1 & \frac{439}{216} & -8 & \frac{3680}{513} 
% & -\frac{845}{4104} & &\\[0.2cm]      \frac{1}{2}& -\frac{8}{27}& 2 & -\frac{3544}{2565} 
% & \frac{1859}{4104}      & -\frac{11}{40} & \\[0.2cm]      \hline      & \frac{25}{216} 
% & 0 & \frac{1408}{2565} & \frac{2197}{4104} &      -\frac{1}{5}      & 0\\[0.2cm]      
% & \frac{16}{135} & 0 & \frac{6656}{12825} & \frac{28561}{56430} & -\frac{9}{50}        
% & \frac{2}{55}\\[0.2cm]      \hline      & \frac{1}{360}& 0 & -\frac{128}{4275} 
% & - \frac{2197}{75240}&      \frac{1}{50} & \frac{2}{55}    \end{array}$$
% 
% con el error:
% 
% $$    ERR = h \left| \frac{1}{360} k_1  - \frac{128}{4275} k_3 -      \frac{2197}{75240} 
% k_4 + \frac{1}{50} k_5 + \frac{2}{55} k_6\right| . $$
% 
% Implementa dicho método llamando la funcion *mirk45fehlberg* con la sintaxis 
%%
% 
%  function  [t,y,ev,hchng_vec,err_vec]=mirk45fehlberg(f,intv,y0,TOL,hmin,hmax)
%
%% 
% * hmin= $10^{-5}$
% * $hmax=\displaystyle\frac{(intv(2)-intv(1))}{50}$
% * TOL=0.01;
%% 
%% Práctica 2 (Euler mejorado-Euler (2-1) (método de extrapolación) 
% Consideramos el siguiente método de extrapolación local con el tablero:
% 
% $$    \begin{array}{l|l}      \begin{array}{l}        {0}	\\        {1}      
% \end{array}      &	       \begin{array}{ll}        0 & 0 \\        1 & 0      
% \end{array}      \\      \hline      y_{n+1}&      \begin{array}{cc}        
% \frac{1}{2}& \frac{1}{2}	      \end{array}      \\      \hline      \hat{y}_{n+1}&      
% \begin{array}{cc}        1& 0      \end{array}    \end{array}$$
% 
% que
%% 
% * avanza con el método de Euler mejorado y
% * estima con el método de Euler.
% * en este caso el método de avance es de orden 2, pero a cambio hay que hacer 
% dos evaluaciones de función por paso.
%% 
% Implementa dicho método llamando la funcion *mieuler21* con la sintaxis 
%%
% 
%  function  [t,y,ev,hchng_vec,err_vec]=mieuler21(f,intv,y0,TOL,hmin,hmax)
%
%% 
% * hmin= $10^{-5}$
% * $hmax=\displaystyle\frac{(intv(2)-intv(1))}{50}$
% * TOL=0.01;
%% Práctica 3: FSAL (Euler-Euler-mejorado (1-2))
% $$      \begin{array}{l|l}        \begin{array}{l}          {0}	\\          
% {1}        \end{array}        &	         \begin{array}{ll}          0 & 0 \\          
% 1 & 0        \end{array}        \\        \hline        y_{n+1}&        \begin{array}{cc}          
% 1& 0        \end{array}        \\        \hline        \hat{y}_{n+1}&        
% \begin{array}{cc}          \frac{1}{2}& \frac{1}{2}        \end{array}      
% \end{array}$$
%% 
% * avanza con el método de Euler y
% * estima con el método de Euler mejorado.
%% 
% Implementa dicho método llamando la funcion *mieuler12fsal* con la sintaxis 
%%
% 
%  function  [t,y,ev,hchng_vec,err_vec]=mieuler12fsal(f,intv,y0,TOL,hmin,hmax)
%
%% 
% * hmin= $10^{-5}$
% * $hmax=\displaystyle\frac{(intv(2)-intv(1))}{50}$
% * TOL=0.01
% * Usad la propiededad *FSAL*
%% Práctica 4 (Método adaptativo de Dormand-Prince FSAL)
% Su tabla está dada por 
% 
% $$\begin{array}{c|ccccccc}0 \\\displaystyle \frac{1}{5} & \displaystyle \frac{1}{5}               
% \\	 \displaystyle \frac{3}{10}     & \displaystyle \frac{3}{40} & \displaystyle 
% \frac{9}{40} \\	 \displaystyle \frac{4}{5} &       \displaystyle \frac{44}{45} 
% & -\displaystyle \frac{56}{15} & \displaystyle \frac{32}{9} \\ \displaystyle 
% \frac{8}{9} & \displaystyle \frac{19372}{6561}&  -\displaystyle \frac{25360}{2187} 
% & \displaystyle \frac{64448}{6561}& -\displaystyle \frac{212}{729} \\1 &      
% \displaystyle \frac{9017}{3168}& -\displaystyle \frac{355}{33}& \displaystyle 
% \frac{46732}{5247}&     \displaystyle \frac{49}{176}&  -\displaystyle \frac{5103}{18656}  
% \\	1 &      \displaystyle \frac{35}{384}&  0&  \displaystyle \frac{500}{1113}& 
% \displaystyle \frac{125}{192}&     -\displaystyle \frac{2187}{6784}& \displaystyle 
% \frac{11}{84}	\\\hline y_{1}&	  \displaystyle \frac{35}{384}& 0& \displaystyle 
% \frac{500}{1113}& \displaystyle \frac{125}{192}& -\displaystyle \frac{2187}{6784}& 
% \displaystyle \frac{11}{84}&  0	\\\hline\widehat  y_{1}&  \displaystyle \frac{5179}{57600}&0&  
% \displaystyle \frac{7571}{16695} & \displaystyle \frac{393}{640} &\displaystyle 
% \frac{92097}{339200}  & \displaystyle \frac{187}{2100} &  \displaystyle \frac{1}{40}	
% \\\end{array}$$
% 
% Implementa dicho método llamando la funcion *midp45sal* con la sintaxis 
%%
% 
%  function  [t,y,ev,hchng_vec,err_vec]=midp45fsal(f,intv,y0,TOL,hmin,hmax)
%
%% 
% * hmin= $10^{-5}$
% * $hmax=\displaystyle\frac{(intv(2)-intv(1))}{50}$
% * TOL=0.01
% * Usad la propiededad *FSAL*
%% Aplicación
% Práctica 5 (Solución que explota)
% Considera el PVI
% 
% $$  \begin{cases}    x'(t)&=x^2(t)\\     x(0)&=1    \end{cases} $$
% 
% La solución exacta es
% 
% $$   x(t)=\frac{1}{1-t}$$
% 
% que es no acotada cuando $t \to 1$.
%% 
% * Usando el método de _Euler explicito_  resuelve el problema en el intervalo  
% $[0 \quad 2]$.
% * Utiliza ahora los 4 metodos adaptativos. 
% * ¿Qué sucede cerca de la discontinuidad que aparece en $t=1$?
%% 
% *Solución*
f = @(t,x) x^2;
intv = [0 2];
y0 = 1;
TOL = 0.01;
hmin = 1e-5; 
hmax = (intv(2)-intv(1))/50;

fac = 0.9;
facmax = 5;

% Pruebo con los 4  metodos adaptativos

[t1, y1, ev1, hchng_vec1, err_vec1] = mirkfehlb45(f, intv, y0, TOL, hmin, hmax, fac, facmax);
[t2, y2, ev2, hchng_vec2, err_vec2] = mieuler21(f, intv, y0, TOL, hmin, hmax, fac, facmax);
[t3, y3, ev3, hchng_vec3, err_vec3] = mieuler12fsal(f, intv, y0, TOL, hmin, hmax, fac, facmax);
[t4, y4, ev4, hchng_vec4, err_vec4] = midp45fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax);

% Hago los plots


figure(1)
plot(t1,y1,'c-')
s1=sprintf('t VS y');
title(s1)

figure(2)
plot(t2,y2,'c-')
s1=sprintf('t VS y');
title(s1)

figure(3)
plot(t3,y3,'c-')
s1=sprintf('t VS y');
title(s1)

figure(4) % Este plot no da lo que debería pero creo que es por el error, ya le he preguntado a enrique
plot(t4,y4,'c-')
s1=sprintf('t VS y');
title(s1)


% figure(1)
% set(gca,'FontSize',16);
% loglog(t1,y1,'bo-',t2,y2,'r*-',t3,y3,'g+-',t4,y4,'ko-')
% s=sprintf('T vs Y \n intv=[0 2] y0=1 \n',intv,y0);
% title(s)
% grid on
% ylabel('Y')
% xlabel('T')
% legend('R-K Fehlberg-4-5','Euler Mejorado 2-1','Euler Mejorado 1-2','Adaptativo dormand-prince ')
% 


%% 
% % Práctica 6 (Ecucacion rigida) 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haz un diagrama de eficiencia en la misma manera como en la *hoja1,* con las 
% siguientes diferencias.
%% 
% * Para hacer un diagrama de eficiencia para un método adaptativo cambia la 
% tolerancia, empezando con $TOL_{initial}=0.01$ y repite el calculo con $TOL_{nuevo}=  
% TOL/2$.
% * comparando el método (con paso fijo) del trapecio (con Newton) con  *mieuler12.m* 
% y *mieuler21.m*.
%% 
% *Solucion*

f=@(t,y)[-2*y(1)+y(2)+2*sin(t); 
    998*y(1)-999*y(2)+999*(cos(t)-sin(t))];
intv=[0 10];
y0=[2;3];

fexact=@(t)[2*exp(-t)+sin(t);2*exp(-t)+cos(t)];


%% Apendice: Las funciones

function [vt,vy,ev,hchng_vec,err_vec,err_vec2]=mirkfehlb45(f,intv,y0,TOL,hmin,hmax,fac,facmax)
disp('H7: file: UB')
    t = intv(1);
    vt = t;
    y = y0;
    vy = y;
    h = hmax;  % Comenzar con el máximo tamaño de paso
    ev = 0;  % Numero de evaluaciones
    hchng_vec = h;  % Vector para guardar los cambios de h
    err_vec = [];  % Vector para guardar los errores
    
    while t < intv(2)
        'entra en el primer bucle'
        % Evaluación de la función con la tabla de Butcher
        k1 = f(t,y);
        k2 = f(t+h/4, y+k1*(h/4));
        k3 = f(t+3*(h/8), y+k1*3*(h/32)+k2*9*(h/32));
        k4 = f(t+12*(h/13), y+k1*1932*(h/2197)-k2*7200*(h/2197)+k3*7296*(h/2197));
        k5 = f(t+h, y+k1*439*(h/216)-k2*8*h+k3*3680*(h/513)-k4*845*(h/4104));
        k6 = f(t+h/2, y-k1*8*(h/27)+k2*2*h-k3*3544*(h/2565)+k4*1859*(h/4104)-k5*11*(h/40));
        'ha pasado por los k'

        % Calcular error
        ERR = h*max(abs(k1/360 - k3*128/4275 - k4*2197/75240 + k5/50 + k6*2/55));
        ev = ev + 6;
        'ha calculado el error'
 
        if ERR < TOL*h
            'ha entrado en el bucle err < Tol'
            %y_next = y + h * (25/216 * k1 + 1408/2565 * k3 + 2197/4104 * k4 - 1/5 * k5);
            y = y + h * (25/216 * k1 + 1408/2565 * k3 + 2197/4104 * k4 - 1/5 * k5);
            vy = [vy, y];  % Almacenar la evolución de y
            t = t + h;  % Aceptar paso
            vt = [vt,t];
            %y = y_next;
            hchng_vec = [hchng_vec, h];
            err_vec = [err_vec, ERR];
        end 
        if ERR == 0
        h = min(hmax, h*facmax);
    else 
        h = min(hmax, h*(min(facmax,fac*(h*(TOL/ERR))^1/5)));
    end
    if t >= intv(2)
        break
    elseif h < hmin
        break
    end
end 
end
%%
function [vt,vy,ev,hchng_vec,err_vec]=mieuler21(f,intv,y0,TOL,hmin,hmax,fac,facmax)
disp('H7: file: UB')
    t = intv(1);
    vt = [t];
    y = y0;
    h = hmax;  % Comienza con el tamaño de paso máximo
    vy = y;  % Vector para registrar la evolución de y
    hchng_vec = h;  % Vector para registrar cambios en h
    err_vec = [];  % Vector para registrar errores estimados
    ev = 0;


    while t < intv(2)

        F1 = f(t, y);
        F2 = f(t+h,y+h*F1);
        y_pred = y + h * F1; % Método de Euler para estimar y_pred  
        y_next = y + (h/2)*(F1 +F2);% ESTO ES Y1' con esti estimo 
        error = abs(y_next - y_pred);% Calcular error
        ev = ev + 2;
        % Control de tamaño de paso según el error
        if error < TOL
            % Si el error es menor que la tolerancia acepta el paso 
            t = t + h;
            y = y_next;
            vt = [vt, t];
            vy = [vy, y];  % Guardar la evolución de y
            hchng_vec = [hchng_vec, h];  % Guardar cambio en h
            err_vec = [err_vec, error];  % Guardar error
        end

        if error == 0
            h = min(hmax, h*facmax); 
        else
            h = min(hmax, h*(min(facmax,fac*(h*(TOL/error))^1/5)));
        end

        if t + h > intv(2)
            h = intv(2) - t;  % Ajustar el último paso para terminar en el límite superior
        end

%         if t >= intv(2)
%              break
%         elseif h < hmin
%              break
%         end
        if h < hmin
             break
        end

     end
end
%% 
% 
function [vt,vy,ev,hchng_vec,err_vec]=mieuler12fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax)
disp('H7: file: UB')
    t = intv(1);
    vt = [t];
    y = y0;
    h = hmax;  % Comienza con el tamaño de paso máximo
    vy = y;  % Vector para registrar la evolución de y
    hchng_vec = h;  % Vector para registrar cambios en h
    err_vec = [];  % Vector para registrar errores estimados
    ev = 0;
    F1 = f(t,y);
    F2 = f(t+h,y+h*F1);

    while t < intv(2)
        y_next = y + h * F1; % Método de Euler para estimar y_pred        
        y_pred = y + (h/2)*(F1 +F2);% ESTO ES Y1' con esti estimo 
        error = abs(y_next - y_pred);% Calcular error
        % Control de tamaño de paso según el error
        if error < TOL
            % Si el error es menor que la tolerancia acepta el paso 
            t = t + h;
            y = y_next;
            vt = [vt, t];
            vy = [vy, y];  % Guardar la evolución de y
            hchng_vec = [hchng_vec, h];  % Guardar cambio en h
            err_vec = [err_vec, error];  % Guardar error
            F1 = F2;
        end

        if error == 0
            h = min(hmax, h*facmax); 
        else
            h = min(hmax, h*(min(facmax,fac*(h*(TOL/error))^1/5)));
        end

        if t + h > intv(2)
            h = intv(2) - t;  % Ajustar el último paso para terminar en el límite superior
        end

        if h < hmin
             break
        end
        F2 = f(t+h,y+h*F1);
        ev = ev + 1;

     end
end
%% 
% 
function [vt,vy,ev,hchng_vec,err_vec]=midp45fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax)
disp('H7: file: UB')
    t = intv(1);
    vt = t;
    y = y0;
    vy = y;
    h = hmax;  % Comenzar con el máximo tamaño de paso
    ev = 0;  % Numero de evaluaciones
    hchng_vec = h;  % Vector para guardar los cambios de h
    err_vec = [];  % Vector para guardar los errores
    k1 = f(t,y); %FSAL
    
    while t < intv(2)
        'entra en el primer bucle'
        % Evaluación de la función con la tabla de Butcher
        k2 = f(t+h/5, y+k1*(h/5));
        k3 = f(t+3*(h/10), y+k1*3*(h/40)+k2*9*(h/40));
        k4 = f(t+4*(h/5), y+k1*44*(h/45)-k2*56*(h/15)+k3*32*(h/9));
        k5 = f(t+8*(h/9), y+k1*19372*(h/6561)-k2*25360*(h/2187)+k3*64448*(h/6561)-k4*212*(h/729));
        k6 = f(t+h, y+k1*9017*(h/3168)-k2*355*(h/33)+k3*46732*(h/5247)+k4*49*(h/176)-k5*5103*(h/18656));
        k7 = f(t+h, y+k1*35*(h/384)+k3*500*(h/1113)+k4*125*(h/192)-k5*2187*(h/6784)+k6*11*(h/84));

        'ha pasado por los k'

        % Calcular error
        ERR = h*max(abs(k1(5179/57600) + k3*(7571/16695) + k4*(393/640) - k5(92097/339200) + k6*(187/2100) + k7/40)); % Este error no se si 
        ev = ev + 5;
        'ha calculado el error'
 
        if ERR < TOL*h
            'ha entrado en el bucle err < Tol'

            y = y + h * (35/385*k1 + 500/1113*k3 + 125/192*k4 - 2187/6784*k5 + 11/84*k6);
            vy = [vy, y];  % Almacenar la evolución de y
            t = t + h;  % Aceptar paso
            vt = [vt,t];
            %y = y_next;
            hchng_vec = [hchng_vec, h];
            err_vec = [err_vec, ERR];
            k1 = k7
        end 

        if ERR == 0
        h = min(hmax, h*facmax);
        else 
        h = min(hmax, h*(min(facmax,fac*(h*(TOL/ERR))^1/5)));
        end

        if t >= intv(2)
        break
        elseif h < hmin
        break
        end
   end 
end
