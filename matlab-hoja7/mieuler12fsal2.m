function [vt,vy,ev,hchng_vec,err_vec]=mieuler12fsal2(f,intv,y0,TOL,hmin,hmax,fac,facmax)
disp('H7: file: UB')
    t = intv(1);
    vt = [t];
    y = y0;
    h = hmax;  % Comienza con el tamaño de paso máximo
    vy = y;  % Vector para registrar la evolución de y
    hchng_vec = h;  % Vector para registrar cambios en h
    err_vec = [];  % Vector para registrar errores estimados
    ev = 0;
    F1 = f(t,y);
    F2 = f(t+h,y+h*F1);

    while t < intv(2)
        y_next = y + h * F1; % Método de Euler para estimar y_pred        
        y_pred = y + (h/2)*(F1 +F2);% ESTO ES Y1' con esti estimo 
        error = abs(y_next - y_pred);% Calcular error
        % Control de tamaño de paso según el error
        if error < TOL
            % Si el error es menor que la tolerancia acepta el paso 
            t = t + h;
            y = y_next;
            vt = [vt, t];
            vy = [vy, y];  % Guardar la evolución de y
            hchng_vec = [hchng_vec, h];  % Guardar cambio en h
            err_vec = [err_vec, error];  % Guardar error
            F1 = F2;
        end

        if error == 0
            h = min(hmax, h*facmax); 
        else
            h = min(hmax, h*(min(facmax,fac*(h*(TOL/error))^1/5)));
        end

        if t + h > intv(2)
            h = intv(2) - t;  % Ajustar el último paso para terminar en el límite superior
        end

        if h < hmin
             break
        end
        F2 = f(t+h,y+h*F1);
        ev = ev + 1;

     end