function [t, vy, ev, hchng_vec, err_vec] = mieuler212(f, intv, y0, TOL, hmin, hmax, fac, facmax)
disp('H7: file: UB')
    t = intv(1);
    y = y0;
    h = hmax;  % Comienza con el tamaño de paso máximo
    vy = y;  % Vector para registrar la evolución de y
    hchng_vec = h;  % Vector para registrar cambios en h
    err_vec = [];  % Vector para registrar errores estimados
    ev = 0;

    while t < intv(2)
        if t + h > intv(2)
            h = intv(2) - t;  % Ajustar el último paso para terminar en el límite superior
        end
        
        y_pred = y + h * f(t, y);  % Método de Euler para estimar y_pred
        y_next = y + (h/2) * (f(t, y) + f(t + h, y_pred));  % Método del trapecio para calcular y_next
        y_hat = y + h * f(t, y);  % Método de Euler para calcular y_hat (estimación)
        error = abs(y_next - y_hat);  % Calcular error
        ev = ev + 4;  % Esta línea parece no tener impacto funcional, podría removerse si no es necesaria
        
        if error < TOL
            t = t + h;  % Aceptar paso
            y = y_next;
            vy = [vy, y];  % Guardar la evolución de y
            hchng_vec = [hchng_vec, h];  % Guardar cambio en h
            err_vec = [err_vec, error];  % Guardar error
            h = min(facmax * h, max(hmin, h * fac * (TOL / error)^0.2));  % Ajustar h para el próximo paso
        else
            h = max(hmin, h * 0.5);  % Reducir h si el error es mayor que la tolerancia
        end
    end
end
