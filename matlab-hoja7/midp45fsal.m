function [t,y,ev,hchng_vec,err_vec,err_vec2]=midp45fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax)
disp('H7: file: UB')
    t = intv(1);
    y = y0;
    h = hmax;  % Comienza con el tamaño de paso máximo
    ev = y;  % Vector para registrar la evolución de y
    hchng_vec = h;  % Vector para registrar cambios en h
    err_vec = [];  % Vector para registrar errores estimados
    k1 = f(t, y); % lo meto fuera del bucle para poder usar k1=k7 luego 

    while t < intv(2)
        if t + h > intv(2)
            h = intv(2) - t;  % Ajustar el último paso para terminar en el límite superior
        end
        
        % Tabla de Butcher
        k2 = f(t + h/5, y + h * (1/5 * k1));
        k3 = f(t + 3*h/10, y + h * (3/40 * k1 + 9/40 * k2));
        k4 = f(t + 4*h/5, y + h * (44/45 * k1 - 56/15 * k2 + 32/9 * k3));
        k5 = f(t + 8*h/9, y + h * (19372/6561 * k1 - 25360/2187 * k2 + 64448/6561 * k3 - 212/729 * k4));
        k6 = f(t + h, y + h * (9017/3168 * k1 - 355/33 * k2 + 46732/5247 * k3 + 49/176 * k4 - 5103/18656 * k5));
        k7 = f(t + h, y + h * (35/384 * k1 + 500/1113 * k3 + 125/192 * k4 - 2187/6784 * k5 + 11/84 * k6));

        % Avance y estimación del error
        y_next = y + h * (35/384 * k1 + 500/1113 * k3 + 125/192 * k4 - 2187/6784 * k5 + 11/84 * k6);
        y_hat = y + h * (5179/57600 * k1 + 7571/16695 * k3 + 393/640 * k4 + 92097/339200 * k5 + 187/2100 * k6 + 1/40 * k7);

        error = norm(y_next - y_hat, inf); % Calculo el error

        if error < TOL
            % Acepto el paso si el error es menor a la tolerancaia
            t = t + h;
            y = y_next;
            ev = [ev, y];  % Guardar la evolución de y
            hchng_vec = [hchng_vec, h];  % Guardar cambio en h
            err_vec = [err_vec, error];  % Guardar error
            
            % Ajustar h para el próximo paso usando la estimación FSAL
            f = fac* (TOL / error)^(1/5);  % Ajuste para el orden del error
            h = min(facmax * h, max(hmin, h * f));  % Ajustar h
            k1 = k7;  % FSAL: Reutilizar k7 como k1 para el próximo paso
        else
            % Reducir el paso si el error es mayor que la tolerancia
            h = max(hmin, h * 0.5);
        end
    end
end