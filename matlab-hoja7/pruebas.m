import mirkfehlb45.*
import mieuler21.*
import mieuler212.*
import mieuler12fsal2.*
import mirkfehlb45fsal.*

f = @(t,x) x^2;
intv = [0 2];
y0 = 1;
TOL = 0.01;
hmin = 1e-5; 
hmax = (intv(2)-intv(1))/50;

fac = 0.9;
facmax = 5;

% Pruebo con los 4  metodos adaptativos
% [t1, y1, ev1, hchng_vec1, err_vec1] = mirkfehlb45(f, intv, y0, TOL, hmin,hmax, fac, facmax); % Ya funciona
% [t2, y2, ev2, hchng_vec2, err_vec2] = mieuler21(f, intv, y0, TOL,hmin,hmax, fac, facmax); % Ya funcionan
%[vt,vy,ev,hchng_vec,err_vec]=mieuler12fsal2(f,intv,y0,TOL,hmin,hmax,fac,facmax) % Ya funciona 

[vt,vy,ev,hchng_vec,err_vec,err_vec2]=mirkfehlb45fsal(f,intv,y0,TOL,hmin,hmax,fac,facmax)
