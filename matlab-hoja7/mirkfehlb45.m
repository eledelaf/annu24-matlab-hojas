function [vt,vy,ev,hchng_vec,err_vec,err_vec2]=mirkfehlb45(f,intv,y0,TOL,hmin,hmax,fac,facmax)
disp('H7: file: UB')
    t = intv(1);
    vt = t;
    y = y0;
    vy = y;
    h = hmax;  % Comenzar con el máximo tamaño de paso
    ev = 0;  % Numero de evaluaciones
    hchng_vec = h;  % Vector para guardar los cambios de h
    err_vec = [];  % Vector para guardar los errores
    
    while t < intv(2)
        'entra en el primer bucle'
        % Evaluación de la función con la tabla de Butcher
        k1 = f(t,y);
        k2 = f(t+h/4, y+k1*(h/4));
        k3 = f(t+3*(h/8), y+k1*3*(h/32)+k2*9*(h/32));
        k4 = f(t+12*(h/13), y+k1*1932*(h/2197)-k2*7200*(h/2197)+k3*7296*(h/2197));
        k5 = f(t+h, y+k1*439*(h/216)-k2*8*h+k3*3680*(h/513)-k4*845*(h/4104));
        k6 = f(t+h/2, y-k1*8*(h/27)+k2*2*h-k3*3544*(h/2565)+k4*1859*(h/4104)-k5*11*(h/40));
        'ha pasado por los k'

        % Calcular error
        ERR = h*max(abs(k1/360 - k3*128/4275 - k4*2197/75240 + k5/50 + k6*2/55));
        ev = ev + 6;
        'ha calculado el error'
 
        if ERR < TOL*h
            'ha entrado en el bucle err < Tol'
            %y_next = y + h * (25/216 * k1 + 1408/2565 * k3 + 2197/4104 * k4 - 1/5 * k5);
            y = y + h * (25/216 * k1 + 1408/2565 * k3 + 2197/4104 * k4 - 1/5 * k5);
            vy = [vy, y];  % Almacenar la evolución de y
            t = t + h;  % Aceptar paso
            vt = [vt,t];
            %y = y_next;
            hchng_vec = [hchng_vec, h];
            err_vec = [err_vec, ERR];
        end 
        if ERR == 0
        h = min(hmax, h*facmax);
    else 
        h = min(hmax, h*(min(facmax,fac*(h*(TOL/ERR))^1/5)));
    end
    if t >= intv(2)
        break
    elseif h < hmin
        break
    end
end 
end
