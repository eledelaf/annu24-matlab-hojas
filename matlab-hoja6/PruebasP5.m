import mibdfnwt.*
f=@(t,y)[-2*y(1)+y(2)+2*sin(t); 
    998*y(1)-999*y(2)+999*(cos(t)-sin(t))];
fexact=@(t)[2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
intv=[0 10];
y0=[2;3];
N = 100;
TOL = 0.1;
nmax = 8;

[tvals, yvals, ev] = mibdfnwt(f, jfunc, intv, y0, N, TOL, nmax);