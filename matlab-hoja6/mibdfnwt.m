function [tvals, yvals, ev] = mibdfnwt(f, jfunc, intv, y0, N, TOL, nmax)
    % Inicialización de vectores y condiciones iniciales
    h = (intv(2) - intv(1)) / N;
    tvals = linspace(intv(1), intv(2), N+1);
    yvals = zeros(length(y0), N+1);
    yvals(:, 1) = y0;
    ev = 0;  % Contador de evaluaciones

    % Asumir y_{n+1} (y1) igual a y0 inicialmente o calcularlo usando otro método simple
    yvals(:, 2) = y0 + h * f(intv(1), y0);  % Esto puede necesitar una mejor estimación inicial

    % Implementación del BDF2 con Iteración de Newton
    for k = 2:N
        y = yvals(:, k);  % y_{n+1}
        yn = yvals(:, k-1);  % y_n
        t = tvals(k+1);
        midiff = 1;
        n = 0;

        while (midiff > TOL) && (n <= nmax)
            % Aplicación del método BDF con Newton
            g = y - (4/3)*yvals(:, k) + (1/3)*yn - (2/3)*h*f(t, y);
            jg = eye(length(y0)) - (2/3)*h*jfunc(t, y);
            delta = jg \ g;
            y_next = y - delta;
            midiff = max(abs(y_next - y));
            y = y_next;
            n = n + 1;
            ev = ev + 1;  % Incremento del contador de evaluaciones
        end

        yvals(:, k+1) = y;
    end
end
