%% Prácticas de Matlab
%% Métodos multipaso
%% Hoja 6
% *Nombre:* Elena 
% 
% *Apellido:* de la Fuente 
% 
% *DNI:* 23821952R
%% 
% %% Práctica 1 Ecuaciones en diferencias 
% Tomando como datos $x_{0}=1, \quad x_1=1.01,\quad N=100$, calculen los términos 
% de la sucesión
% 
% $$    \begin{cases}      x_{n+2} & = {7 \over 3} x_{n+1} - {2 \over 3} x_{n} 
% \\      & x_{0}, \ x_{1} \quad \mbox{dados}    \end{cases}$$
% 
% para $n=0, \ldots, N$. Los resultados han de almacenarse en la tabla $x$. 
% Además haz una gráfica de $x_{n}$ contra $n$.
% 
% *Solución:*
disp('H6: código de Elena')

x = [1 1.01]
N = 100
for i = 3:1:N
    x(i) = 7/3*x(i-1) -2/3*x(i-2) 
end

figure;
set(gca, 'FontSize', 16);
hold on;
plot(1:N,x, 'b +');
hold off;
grid on;
title(sprintf('Hoja 6 Ej 1'));

%% Práctica 2 Leap frog (Ecuación escalar)
% Consideramos el método de Leap-Frog (punto medio).
% 
% $$    y_{n+2}-y_n = 2hf(t_{n+1},y_{n+1}) $$
% 
% Considerad la EDO
% 
% $$\begin{cases}y^{\prime}&=\lambda y\\y(0)&=1\\\end{cases}$$
% 
% $\lambda = -20$, resolved dicha EDO con el metodo de Leap-frog (usando el 
% método de Euler modificado para inicializarlo), con $N=100$, $N=1000$, y $N=10000$. 
% Pintad la solucion $y$ frente a $t$.
% 
% *Solución:*
disp('H6: código de Elena')
import mieulermod.*

f = @(t,y)[(-20)*y];
fexact = @(t,y)[exp(t)];
y0 = 1;
intv = [0 1];
N = 100;
M = 3;


N_vect=[];
N0=N;
error_vect=[];
for k=1:M
    [t,y] = mileapfrog(f,intv,y0,N);
    solexact1 = fexact(t);
    error1 = max(max(abs(solexact1-y)));
    error_vect = [error_vect,error1];
    N_vect = [N_vect,N];
    N=N0*10^k;

    figure(k)
    set(gca,'FontSize',16)
    plot(t,y,'b*-')
    s=sprintf('N VS Error max \n y0=[2 3]  intv=[0 10]  N=200  M=3  met=mileapfrog \n');
    grid on
    ylabel('y')
    xlabel('t')
    legend('Leapfrog')
end




%% Práctica 3 Leap frog (Sistemas de ecuaciones) 
% Consideramos el método de Leap-frog.
% 
% $$    y_{n+2}-y_n = 2hf(t_{n+1},y_{n+1}) $$
% 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$    A=  \left(      \begin{array}{cc}        -2 & 1\\        1 & -2      
% \end{array}    \right)    \qquad      B(t) =          \left(            \begin{array}{cc}              
% 2\sin(t)\\              2(\cos(t)-\sin(t)            \end{array}          \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
%% 
% * Haz un diagrama de eficiencia (solo para $h$) en la misma manera como en 
% la hoja anterior 
%% 
% *Solución:*
%close all
%clear all
disp('H6: código de Elena')
f = @(t,y)[-2*y(1)+y(2)+2*sin(t); y(1)-2*y(2)+2*(cos(t)-sin(t))];
fexact = @(t)[2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
intv = [0 10];
y0 = [2;3];
N = 100;
M=7;

met=@mileapfrog;
[N_vect,error_vect]=fcomparerror(met,f,intv,y0,N,fexact,M)

figure(8)
set(gca,'FontSize',16);
loglog(N_vect,error_vect,'g*-')
s=sprintf('N VS Error max \n y0=[2 3]  intv=[0 10]  N=200  M=7  met=mileapfrog \n');
title(s)
grid on
ylabel('ERROR')
xlabel('N')
legend('Leapfrog')


%% 
% %% 
% * además  $N=1000$ dibuja el error  (es decir $\log\left(\|y(t_n)-y_n\|_{\infty}\right)$ 
% pero no $\log\left(\max(\max(|(y(t_n-y_n)|)))\right)$) frente la variable $t$.
%% 
% *Solución:*
disp('H6: código de Elena')
f=@(t,y)[-2*y(1)+y(2)+2*sin(t); y(1)-2*y(2)+2*(cos(t)-sin(t))];
fexact=@(t)[2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
intv=[0 10];
y0=[2;3];
N=1000;
met=@mileapfrog;

[tvals,error_vect]=fcomparerrort(met,f,intv,y0,N,fexact)

i=4;
figure(i)
set(gca,'FontSize',16);
loglog(tvals,error_vect,'b-')
s=sprintf('t VS Error max \n y0=[2 3]  intv=[0 10]  N=1000  met=mileapfrog \n');
title(s)
grid on
ylabel('ERROR')
xlabel('t')
legend('Leapfrog')



%% Práctica 5 BDF 
% Implementa el método *BDF*
% 
% $$  y_{n+2}-\frac{4}{3} y_{n+1}+\frac{1}{3} y_n =\frac{2}{3} h f_{n+2}\,.$$
% 
% *Observación:*
%% 
% * Inicializa el método con un método implícito del mismo orden.
% * En cada paso tienes que resolver una ecuación implícita  $z=g(h,x,z)$. Usa 
% la idea de iteración tipo Newton.
%% 
% Considerar el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haz un diagrama de eficiencia (solo para $h$) en la misma manera como en la 
% practica anterior
% 
% *Solución:*

%close all 
%clear all
disp('H6: código de alumno')

f=@(t,y)[-2*y(1)+y(2)+2*sin(t); 
    998*y(1)-999*y(2)+999*(cos(t)-sin(t))];
fexact=@(t)[2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
intv=[0 10];
y0=[2;3];
M = 8
N_vect = 200 .* (2.^(0:M));
h_vect =[];
% for i = N_vect
%     h_vect = [h_vect , (intv(2)-intv(1))/(i)];
% end
% 
% for k = 1:M
% 
% 
% end
% 
% 

%% Apéndice: Las funciones Leap_frog y BDF2

function [t,y]=mileapfrog(f,intv,y0,N)
h=(intv(2)-intv(1))/N;
t = linspace(intv(1), intv(2), N+1);
yn1=y0+(h/2)*(f(t(1),y0)+f(t(2),y0+h*f(t(1),y0)));
y=[y0,yn1];
yn=y0;
for k=2:N
    yn2=yn+2*h*f(t(k),yn1);
    y=[y,yn2];
    yn=yn1;
    yn1=yn2;
end
end

function [t,y] = mibdfnwt(f,intv,y0,N)
h=(intv(2)-intv(1))/N;
t = linspace(intv(1), intv(2), N+1);

%Inicializacion
y = zeros(length(y0),N+1);
y(:,1) = y0;

for k = 2:N

end


end

function [N_vect,error_vect]=fcomparerror(met,f,intv,y0,N,fexact,M)
N_vect=[];
N0=N;
error_vect=[];
for k=1:M
    [tvals,yvals]=met(f,intv,y0,N);
    solexact1=fexact(tvals);
    error1=max(max(abs(solexact1-yvals)));
    error_vect=[error_vect,error1];
    N_vect=[N_vect,N];
    N=N0*(2^(k));
end
end

function [tvals,error_vect]=fcomparerrort(met,f,intv,y0,N,fexact)
error_vect=[];
    [tvals,yvals]=met(f,intv,y0,N);
    solexact1=fexact(tvals);
    error1=max(abs(solexact1-yvals));
    error_vect=[error_vect,error1];
end
