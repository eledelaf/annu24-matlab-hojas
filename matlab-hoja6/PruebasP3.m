% Pruebas parte 3

% Definición de la función f(t, y)
f = @(t, y) [-2*y(1) + y(2) + 2*sin(t); 998*y(1) - 999*y(2) + 999*(cos(t) - sin(t))];

% Función de la solución exacta
fexact = @(t) [2*exp(-t) + sin(t); 2*exp(-t) + cos(t)];

% Intervalo de tiempo y condición inicial
tspan = [0 10];
y0 = [2; 3];
N = 100; % Número de pasos
h = (tspan(2) - tspan(1)) / N; % Calcula h basado en el número de pasos

% Preparar arrays para almacenar soluciones
t = linspace(tspan(1), tspan(2), N+1);
y = zeros(2, N+1);
y(:,1) = y0;

% Implementación del método BDF de segundo orden
for i = 2:N+1
    if i == 2 % Solo en el primer paso usa el método implícito inicial
        % Aquí debes incluir el código para el primer paso usando el método implícito
    else
        % Definir la función para la iteración de Newton
        F = @(z) z - (4/3)*y(:,i-1) + (1/3)*y(:,i-2) - (2/3)*h*f(t(i), z);
        J = @(z) eye(2) - (2/3)*h*[-2, 1; 998, -999]; % Jacobiano de F
        
        % Iteración de Newton para encontrar z
        z = y(:,i-1); % Estimación inicial
        for iter = 1:10 % Ajusta el número de iteraciones según la convergencia deseada
            delta = J(z)\F(z);
            z = z - delta;
            if norm(delta, 'inf') < 1e-6
                break;
            end
        end
        
        % Actualizar los valores
        y(:,i) = z;
    end
end

% Comparar con la solución exacta y graficar
y_exact_vals = arrayfun(fexact, t, 'UniformOutput', false);
y_exact_vals = [y_exact_vals{:}]; % Transformar celdas a matriz

% Graficar la solución numérica y la solución exacta
figure;
plot(t, y(1,:), 'b-', t, y_exact_vals(1,:), 'r--');
hold on;
plot(t, y(2,:), 'g-', t, y_exact_vals(2,:), 'k--');
legend('y1 num', 'y1 exact', 'y2 num', 'y2 exact');
xlabel('Tiempo t');
ylabel('Soluciones y1, y2');
title('Comparación entre la solución numérica y la exacta');

% Diagrama de eficiencia
% Implementa el código para evaluar el método con diferentes pasos de tiempo y graficar el error
