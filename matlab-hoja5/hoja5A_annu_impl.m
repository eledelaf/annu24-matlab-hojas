%% Prácticas de Matlab
%% Resolución de EDO con métodos implícitos
%% Hoja 5 A
% *Nombre:* Elena
% 
% *Apellido:* de la Fuente
% 
% *EMAIL:* eledelaf@ucm.es
% 
% *DNI:* 23821952R
% 
% %% 1. Implementación de métodos implícitos
%% Práctica 1 (Implementación del método de Euler implícito)
% Escribid en el apéndice A1 4 funciones que implementen el método de Euler 
% (implícito) 
% 
% $$      \left\{\begin{array}{l}               y_{i+1}=y_i + h f(t_{i+1},y_{i+1}) 
% \quad i=0,\ldots ,N-1          \\               y_0 \approx a        \end{array}$$
% 
% para el PVI (problema de valor inicial para sistemas de EDOs) en varias maneras 
% Existen las siguientes maneras de implementar dicho método, dependiendo de 
%% 
% * qué tipo de iteración usas para resolver la relación implícita 
% * cómo eliges el dato inicial para esa iteración.
%% 
% En cualquier caso, tenéis que implementar la iteración usando un
%% 
% * *while* y una tolerancia dada.
% * un numero máximo de iteraciones para evitar un bucle infinito.
%% 
% La iteración puede ser bien esto, bien esto
%% 
% * Una iteración simple.
% * Una iteración tipo Newton
%% 
% La elección del punto inicial para la iteración puede ser:
%% 
% * el valor del paso anterior. 
% * el valor calculado por un método explícito, como por ejemplo Euler explícito. 
% (Conocido como  *predictor-corrector*)
%% 
% Por lo tanto, son posibles las siguientes implementaciones:
%% 
% * mieulerimpfix: Iteración simple+dato inicial el valor del paso anterior, 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfix(f,intv,y0,N,TOL,nmax)
%
%% 
% * mieulernwt: Iteración tipo Newton+dato inicial el valor del paso anterior 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpnwt(f,jf,intv,y0,N,TOL,nmax)
%
%% 
% * meulerfixpc: Iteración simple+dato inicial por el método de Euler y que 
% responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax)
%
%% 
% * mieulernwtpc: Iteración tipo Newton+dato inicial por el método de Euler 
% y que responda a la sintaxis
%%
% 
%     [t,y]=mieulerimpfixpc(f,jf,intv,y0,N,TOL,nmax)
%
%% Práctica 2 (El método del trapecio)
% Repetid el ejercicio anterior implementando el método del trapecio
%% Práctica 3 (Ecuación no rígida con Euler implícito)
% Considerad el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$    A=  \left(      \begin{array}{cc}        -2 & 1\\        1 & -2      
% \end{array}    \right)    \qquad      B(t) =          \left(            \begin{array}{cc}              
% 2\sin(t)\\              2(\cos(t)-\sin(t)            \end{array}          \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haced un diagrama de eficiencia (solo para $N$) en la misma manera como en 
% la práctica anterior
%% 
clear all
disp('H5: codigo de Elena')
f = @(t, y)[-2*y(1)+y(2) + 2*sin(t);y(1)-2*y(2)+2*(cos(t)-sin(t))];
y0=[2;3];
intv=[0 10];
nmax=10;
M=7;
TOL=0.01;
yexact = @(t)[2*exp(-t) +  sin(t); 2*exp(-t)+cos(t)];
jfunc=@(t,y)[-2 1;1 -2];
N_vect = 100 .* (2.^(0:M)); % Creo el vector N, N0 = 100, Nj=N0 * 2^j, j = 0,...,7
N = 100;

%%
% Voy a sacar yeuler de mieulerimpnwt, mieulerimpfix, mieulerimpfixpc, mieulerimpnwtpc
[teuler,yeuler,ev]=mieulerimpfix(f,intv,y0,N,TOL,nmax);
[teulerpc,yeulerpc,ev]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax);
[tvals, yvals,ev]=mieulerimpnwt(f,jfunc,intv,y0,N,TOL,nmax);
[tvalspc, yvalspc,ev]=mieulerimpnwtpc(f,jfunc,intv,y0,N,TOL,nmax);

%%
% Voy a sacar los vectores de error en función de N
met1=@mieulerimpnwt;
[N_vect1,~,error_vect1]=fcomparerrornwt(met1,f,jfunc,intv,y0,N,yexact,M,TOL,nmax);

met2=@mieulerimpfix;
[N_vect2,~,error_vect2]=fcomparerrorfix(met2,f,intv,y0,N,yexact,M,TOL,nmax);

met3=@mieulerimpnwtpc
[N_vect3,~,error_vect3]=fcomparerrornwt(met3,f,jfunc,intv,y0,N,yexact,M,TOL,nmax);

met4=@mieulerimpfixpc;
[N_vect4,~,error_vect4]=fcomparerrorfix(met4,f,intv,y0,N,yexact,M,TOL,nmax);

%%
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfix* 
figure(1)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect2,error_vect2,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mieulerimpnwt','mieulerimpfix')

%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfixpc*
figure(2)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect4,error_vect4,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mieulerimpnwt','mieulerimpfixpc')

%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpnwtpc*
figure(3)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect3,error_vect3,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mieulerimpnwt','mieulerimpnwtpc')

%% 
% * calcula la pendiente de las rectas
% Calculo la pendiente de mieulerimpfix
y1 = yeuler(:,1);
y2 = yeuler(:,2);
peneuler = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1)))

% Calculo la pendiente de mieulerimpfixpc
y3 = yeulerpc(:,1);
y4 = yeulerpc(:,2);
peneulerpc = (log(y4(2))-log(y3(2)))/(log(y4(1))-log(y3(1)))

% Calculo la pendiente de mieulerimpnwt
y5 = yvals(:,1);
y6 = yvals(:,2);
peneuler = (log(y6(2))-log(y5(2)))/(log(y6(1))-log(y5(1)))

% Calculo la pendiente de mieulerimpfixpc
y7 = yvalspc(:,1);
y8 = yvalspc(:,2);
peneulerpc = (log(y8(2))-log(y7(2)))/(log(y8(1))-log(y7(1)))

%% Práctica 4 (Ecuación no rígida con el trapecio)
% Repetid la práctica 3 pero con el método del trapecio de la práctica 2.
clear all
disp('H5: codigo de Elena')
f = @(t, y)[-2*y(1)+y(2) + 2*sin(t);y(1)-2*y(2)+2*(cos(t)-sin(t))];
y0=[2;3];
intv=[0 10];
nmax=10;
M=7;
TOL=0.01;
yexact = @(t)[2*exp(-t) +  sin(t); 2*exp(-t)+cos(t)];
jfunc=@(t,y)[-2 1;1 -2];
N_vect = 100 .* (2.^(0:M)); % Creo el vector N, N0 = 100, Nj=N0 * 2^j, j = 0,...,7
N = 100;

%%
% Voy a sacar yeuler de mieulerimpnwt, mieulerimpfix, mieulerimpfixpc, mieulerimpnwtpc
[ttrap,yeuler,ev]=mitrapfix(f,intv,y0,N,TOL,nmax);
[ttrapc,yeulerpc,ev]=mitrapfixpc(f,intv,y0,N,TOL,nmax);
[tvals, yvals,ev]=mitrapnwt(f,jfunc,intv,y0,N,TOL,nmax);
[tvalspc, yvalspc,ev]=mitrapnwtpc(f,jfunc,intv,y0,N,TOL,nmax);

%%
% Voy a sacar los vectores de error en función de N
met1=@mitrapnwt;
[N_vect1,~,error_vect1]=fcomparerrornwt(met1,f,jfunc,intv,y0,N,yexact,M,TOL,nmax);

met2=@mitrapfix;
[N_vect2,~,error_vect2]=fcomparerrorfix(met2,f,intv,y0,N,yexact,M,TOL,nmax);

met3=@mitrapnwtpc
[N_vect3,~,error_vect3]=fcomparerrornwt(met3,f,jfunc,intv,y0,N,yexact,M,TOL,nmax);

met4=@mitrapfixpc;
[N_vect4,~,error_vect4]=fcomparerrorfix(met4,f,intv,y0,N,yexact,M,TOL,nmax);

%%
% * comparando los métodos de *mitrapnwt*, *mitrapfix* 
figure(4)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect2,error_vect2,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mitrapnwt','mitrapfix')

%% 
% * comparando los métodos de *mitrapnwt*, *mitrapfixpc*
figure(5)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect4,error_vect4,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mitrapnwt','mitrapfixpc')

%% 
% * comparando los métodos de *mitrapnwt*, *mitrapnwtpc*
figure(6)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect3,error_vect3,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mitrapnwt','*mitrapnwtpc')

%% 
% * calcula la pendiente de las rectas
% Calculo la pendiente de mieulerimpfix
y1 = yeuler(:,1);
y2 = yeuler(:,2);
peneuler = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1)))

% Calculo la pendiente de mieulerimpfixpc
y3 = yeulerpc(:,1);
y4 = yeulerpc(:,2);
peneulerpc = (log(y4(2))-log(y3(2)))/(log(y4(1))-log(y3(1)))

% Calculo la pendiente de mieulerimpnwt
y5 = yvals(:,1);
y6 = yvals(:,2);
peneuler = (log(y6(2))-log(y5(2)))/(log(y6(1))-log(y5(1)))

% Calculo la pendiente de mieulerimpfixpc
y7 = yvalspc(:,1);
y8 = yvalspc(:,2);
peneulerpc = (log(y8(2))-log(y7(2)))/(log(y8(1))-log(y7(1)))

%% Práctica 5 (Ecuación rígida con Euler implícito)
% Considerad el siguiente sistema 
% 
% $$  y^{\prime}(t)  =  Ay(t) + B(t) \quad t\in [0,10]$$
% 
% $$  \left(   A=  \begin{array}{cc}    -2 & 1\\    998 & -999   \end{array}   
% \right)  \quad  B(t)=\left(   \begin{array}{c}    2\sin(t)\\    999(\cos(t)-\sin(t))  
% \end{array} \right)$$
% 
% $$          y(0)=          \left(             \begin{array}{c}              
% 2\\              3            \end{array}          \right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}   \left(     \begin{array}{c}      1\\      1    \end{array}  
% \right)  +  \left(     \begin{array}{c}      \sin(t)\\      \cos(t)    \end{array}  
% \right)$$
% 
% Haced un diagrama de eficiencia (solo para $N$) en la misma manera como en 
% la práctica anterior
%%
clear all
f=@(t,y)[-2*y(1)+y(2)+2*sin(t); 998*y(1)-999*y(2)+999*(cos(t)-sin(t))];
jfunc=@(t,y)[-2 1;998 -999];
fexact=@(t)[2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
intv=[0 10];
y0=[2;3];
N=5000;
nmax=10;
M=7;
TOL=0.01;
%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfix* 
[N_vect1,~,error_vect1]=fcomparerrornwt(@mieulerimpnwt,f,jfunc,intv,y0,N,fexact,M,TOL,nmax)
[N_vect2,~,error_vect2]=fcomparerrorfix(@mieulerimpfix,f,intv,y0,N,fexact,M,TOL,nmax)

figure(7)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect2,error_vect2,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mieulerimpnwt','mieulerimpfix')

%% 
% * comparando los métodos de *mieulerimpnwt*, *mieulerimpfixpc*
[N_vect3,~,error_vect3]=fcomparerrorfix(@mieulerimpfixpc,f,intv,y0,N,fexact,M,TOL,nmax)

figure(8)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect3,error_vect3,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mieulerimpnwt','mieulerimpfixpc')

%% 
% * comparando los métodos de *mieulerimpnwtpc*, *mieulerimpnwtpc*
[N_vect4,~,error_vect4]=fcomparerrornwt(@mieulerimpnwt,f,jfunc,intv,y0,N,fexact,M,TOL,nmax)

figure(9)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect4,error_vect4,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mieulerimpnwtpc','mieulerimpnwtpc')

%% 
% * calcula la pendiente de las rectas
[teuler,yeuler,ev]=mieulerimpfix(f,intv,y0,N,TOL,nmax);
[teulerpc,yeulerpc,ev]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax);
[tvals, yvals,ev]=mieulerimpnwt(f,jfunc,intv,y0,N,TOL,nmax);
[tvalspc, yvalspc,ev]=mieulerimpnwtpc(f,jfunc,intv,y0,N,TOL,nmax);

% Calculo la pendiente de mieulerimpfix
y1 = yeuler(:,1);
y2 = yeuler(:,2);
peneuler = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1)))

% Calculo la pendiente de mieulerimpfixpc
y3 = yeulerpc(:,1);
y4 = yeulerpc(:,2);
peneulerpc = (log(y4(2))-log(y3(2)))/(log(y4(1))-log(y3(1)))

% Calculo la pendiente de mieulerimpnwt
y5 = yvals(:,1);
y6 = yvals(:,2);
peneuler = (log(y6(2))-log(y5(2)))/(log(y6(1))-log(y5(1)))

% Calculo la pendiente de mieulerimpfixpc
y7 = yvalspc(:,1);
y8 = yvalspc(:,2);
peneulerpc = (log(y8(2))-log(y7(2)))/(log(y8(1))-log(y7(1)))


%% Práctica 6 (Ecuación rígida con el trapecio)
% Repetid la práctica 5 pero con el método del trapecio de la práctica 2.
% 
% *Solución *
clear all
f=@(t,y)[-2*y(1)+y(2)+2*sin(t); 998*y(1)-999*y(2)+999*(cos(t)-sin(t))];
jfunc=@(t,y)[-2 1;998 -999];
fexact=@(t)[2*exp(-t)+sin(t);2*exp(-t)+cos(t)];
intv=[0 10];
y0=[2;3];
N=5000;
nmax=10;
M=7;
TOL=0.01;
%% 
% * comparando los métodos de *mitrapnwt*, *mitrapfix* 
[N_vect1,~,error_vect1]=fcomparerrornwt(@mitrapnwt,f,jfunc,intv,y0,N,fexact,M,TOL,nmax)
[N_vect2,~,error_vect2]=fcomparerrorfix(@mitrapfix,f,intv,y0,N,fexact,M,TOL,nmax)

figure(7)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect2,error_vect2,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mitrapnwt','mitrapfix')

%% 
% * comparando los métodos de *mitrapnwt*, *mitrapfixpc*
[N_vect3,~,error_vect3]=fcomparerrorfix(@mitrapfixpc,f,intv,y0,N,fexact,M,TOL,nmax)

figure(8)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect3,error_vect3,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mitrapnwt','mitrapfixpc')

%% 
% * comparando los métodos de *mitrapnwtpc*, *mitrapnwtpc*
[N_vect4,~,error_vect4]=fcomparerrornwt(@mitrapnwt,f,jfunc,intv,y0,N,fexact,M,TOL,nmax)

figure(9)
set(gca,'FontSize',16);
loglog(N_vect1,error_vect1,'bo-',N_vect4,error_vect4,'r*-')
s=sprintf('Error maximo vs N \n problema no stiff \n intv=[0 10] y0=[2 3]\n',intv,y0);
title(s)
grid on
ylabel('N')
xlabel('Error maxmax')
legend('mitrapnwtpc','mitrapnwtpc')

%% 
% * calcula la pendiente de las rectas
[ttrap,ytrap1,ev]=mitrapfix(f,intv,y0,N,TOL,nmax);
[ttrap,ytrap2,ev]=mitrapfixpc(f,intv,y0,N,TOL,nmax);
[ttrap,ytrap3,ev]=mitrapnwt(f,jfunc,intv,y0,N,TOL,nmax);
[ttrap,ytrap4,ev]=mitrapnwtpc(f,jfunc,intv,y0,N,TOL,nmax);

% Calculo la pendiente de mieulerimpfix
y1=ytrap1(:,1);
y2=ytrap1(:,2);
peneuler = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1)))

% Calculo la pendiente de mieulerimpfixpc
y3=ytrap2(:,1);
y4=ytrap2(:,2);
peneulerpc = (log(y4(2))-log(y3(2)))/(log(y4(1))-log(y3(1)))

% Calculo la pendiente de mieulerimpnwt
y5=ytrap3(:,1);
y6=ytrap3(:,2);
peneuler = (log(y6(2))-log(y5(2)))/(log(y6(1))-log(y5(1)))

% Calculo la pendiente de mieulerimpfixpc
y7=ytrap4(:,1);
y8=ytrap4(:,2);
peneulerpc = (log(y8(2))-log(y7(2)))/(log(y8(1))-log(y7(1)))

% 
%% Apéndice: la implementación de las prácticas 1+2
function [teuler,yeuler,ev]=mieulerimpfix(f,intv,y0,N,TOL,nmax) 
teuler=intv(1); % Inicializo el vector de tiempo 
yeuler=y0; % Inicializo el vector del resultado
y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
for k=1:N 
    t=intv(1)+h*k; % siguiente paso 
    teuler=[teuler,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
    while (midiff>TOL) && (n<=nmax)
        y1=yant+h*f(t,y); 
        midiff=max(max(abs(y1-y)));
        y=y1;
        n=n+1;
        e=e+1;
    end
    yant = y;
    yeuler=[yeuler,y]; 
    ev=[ev,e];
end
end
%%
function [teuler,yeuler,ev]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax) 
teuler=intv(1); % Inicializo el vector de tiempo 
yeuler=y0; % Inicializo el vector del resultado 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
t = intv(1);
for k=1:N 
    y = yant + h*f(t,yant);%Predictor corrector 
    t=intv(1) + h*k; % siguiente paso 
    teuler=[teuler,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
    while (midiff>TOL) && (n<=nmax)
        y1=yant+(h/2)*(f(t,yant)+f(t+h,y)); 
        midiff=max(max(abs(y1-y)));
        y=y1;
        n=n+1;
        e=e+1;
    end
    yant = y;
    yeuler=[yeuler,y]; 
    ev=[ev,e];
end
end
%%
function [tvals, yvals,ev]=mieulerimpnwt(f,jfunc,intv,y0,N,TOL,nmax)
yvals = 0;
tvals=intv(1); % Inicializo el vector de tiempo 
yvals=y0; % Inicializo el vector del resultado
y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
for k=1:N 
    t=intv(1)+h*k; % siguiente paso 
    tvals=[tvals,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
      while (midiff>TOL) && (n<=nmax)
        y2=y -(yant+h*f(t,y)); % lo calculo para la iteracion newtoniana
        % Hago la inversa
        %l = length(y0)
        jg = eye(length(y0)) - h*jfunc(t, y);
        y3 = y - (jg\y2);
        midiff=max(max(abs(y3-y)));
        y = y3;
        n=n+1;
        e=e+1;
      end
    yant = y;
    yvals=[yvals,y]; 
    ev=[ev,e];
end
end
%%
function [tvals, yvals,ev]=mieulerimpnwtpc(f,jfunc,intv,y0,N,TOL,nmax)
tvals=intv(1); % Inicializo el vector de tiempo 
yvals=y0; % Inicializo el vector del resultado
%y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
t = intv(1);
for k=1:N 
    y = yant + h*f(t,yant);%Predictor corrector 
    t=intv(1) + h*k; % siguiente paso 
    tvals=[tvals,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
      while (midiff>TOL) && (n<=nmax)
        y2=y -(yant+h*f(t,y)); % lo calculo para la iteracion newtoniana
        % Hago la inversa
        %l = length(y0)
        jg = eye(length(y0)) - h*jfunc(t, y);
        y3 = y - (jg\y2);
        midiff=max(max(abs(y3-y)));
        y = y3;
        n=n+1;
        e=e+1;
      end
    yant = y;
    yvals=[yvals,y]; 
    ev=[ev,e];
end
end
%%
function [ttrap,ytrap,ev]=mitrapfix(f,intv,y0,N,TOL,nmax)
ttrap=intv(1); % Inicializo el vector de tiempo 
ytrap=y0; % Inicializo el vector del resultado
y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h

for k=1:N 
    t=intv(1)+h*k; % siguiente paso 
    ttrap=[ttrap,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;

    while (midiff>TOL) && (n<=nmax)
        y1=yant+(h/2)*(f(t,yant)+f(t+h,y)); 
        midiff=max(max(abs(y1-y)));
        y=y1;
        n=n+1;
        e=e+1;
    end
    yant = y;
    ytrap=[ytrap,y]; 
    ev=[ev,e];
end
end
%%
function [ttrap,ytrap,ev]=mitrapfixpc(f,intv,y0,N,TOL,nmax)
ttrap=intv(1); % Inicializo el vector de tiempo 
ytrap=y0; % Inicializo el vector del resultado
%y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
t = intv(1);
for k=1:N 
    y = yant + h*f(t,yant);%Predictor corrector 
    t=intv(1) + h*k; % siguiente paso 
    ttrap=[ttrap,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
    while (midiff>TOL) && (n<=nmax)
        y1=yant+(h/2)*(f(t,yant)+f(t+h,y)); 
        midiff=max(max(abs(y1-y)));
        y=y1;
        n=n+1;
        e=e+1;
    end
    yant = y;
    ytrap=[ytrap,y]; 
    ev=[ev,e];
end
end
%%
function [ttrap,ytrap,ev]=mitrapnwt(f,jfunc,intv,y0,N,TOL,nmax)
ttrap=intv(1); % Inicializo el vector de tiempo 
ytrap=y0; % Inicializo el vector del resultado
y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h

for k=1:N 
    t=intv(1)+h*k; % siguiente paso 
    ttrap=[ttrap,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
      while (midiff>TOL) && (n<=nmax) % lo calculo para la iteracion newtoniana
        y2 = y -(yant+(h/2)*(f(t,yant)+f(t+h,y)));
        % Hago la inversa
        %l = length(y0)
        jg = eye(length(y0)) - h*jfunc(t, y);
        y3 = y - (jg\y2);
        midiff=max(max(abs(y3-y)));
        y = y3;
        n=n+1;
        e=e+1;
      end
    yant = y;
    ytrap=[ytrap,y]; 
    ev=[ev,e];
end
end
%%
function [ttrap,ytrap,ev]=mitrapnwtpc(f,jfunc,intv,y0,N,TOL,nmax)
ttrap=intv(1); % Inicializo el vector de tiempo 
ytrap=y0; % Inicializo el vector del resultado 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
t = intv(1);
for k=1:N 
    y = yant + h*f(t,yant);%Predictor corrector 
    t=intv(1) + h*k;
    ttrap=[ttrap,t]; 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
      while (midiff>TOL) && (n<=nmax)
        y2 = y -(yant+(h/2)*(f(t,yant)+f(t+h,y)));
        jg = eye(length(y0)) - h*jfunc(t, y);
        y3 = y - (jg\y2);
        midiff=max(max(abs(y3-y)));
        y = y3;
        n=n+1;
        e=e+1;
      end

    yant = y;
    ytrap=[ytrap,y]; 
    ev=[ev,e];
end
end
%%
function [N_vect,Ev_vect,error_vect]=fcomparerrorimpnwt(met,func,jacfunc,intv,y0,N,yexact,M,TOL,nmax);
disp('H4: file: fcomparerrorimpnwt Elena')
N0=N;
error_vect=0;
N_vect=N;
Ev_vect=0;
for k=1:M
    [tvals,yvals,ev]=met(f,jfunc,intv,y0,N,TOL,nmax);
    solexact1=fexact(tvals);
    error1=max(max(abs(solexact1-yvals)));
    error_vect=[error_vect,error1];
    Ev_vect=[Ev_vect,ev];
    N=N0*(2^(k));
    N_vect=[N_vect,N];
end
end 

function [N_vect,Ev_vect,error_vect]=fcomparerrorimpfix(met,func,intv,y0,N,yexact,M,TOL,nmax);
disp('H4: file: fcomparerrorimpfix Elena')
N0=N;
error_vect=0;
N_vect=N;
Ev_vect=0;
for k=1:M
    [tvals,yvals,ev]=met(f,intv,y0,N,TOL,nmax);
    solexact1=fexact(tvals);
    error1=max(max(abs(solexact1-yvals)));
    error_vect=[error_vect,error1];
    Ev_vect=[Ev_vect,ev];
    N=N0*(2^(k));
    N_vect=[N_vect,N];
end
end

function [p,q]=fcalcorden(N_vect,error_vect) 
disp('H4: file: fcalcorden Elena')
end
