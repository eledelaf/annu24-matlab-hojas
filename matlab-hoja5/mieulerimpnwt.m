function [teuler, yeuler,ev]=mieulerimpnwt(f,jfunc,intv,y0,N,TOL,nmax)
teuler=intv(1); % Inicializo el vector de tiempo 
yeuler=y0; % Inicializo el vector del resultado
y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
for k=1:N 
    t=intv(1)+h*k; % siguiente paso 
    teuler=[teuler,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
      while (midiff>TOL) && (n<=nmax)
        y2=y -(yant+h*f(t,y)); % lo calculo para la iteracion newtoniana
        % Hago la inversa
        %l = length(y0)
        jg = eye(length(y0)) - h*jfunc(t, y);
        y3 = y - (jg\y2);
        midiff=max(max(abs(y3-y)));
        y = y3;
        n=n+1;
        e=e+1;
      end
    yant = y;
    yeuler=[yeuler,y]; 
    ev=[ev,e];
end
end