function [ttrap,ytrap,ev]=mitrapnwt(f,jfunc,intv,y0,N,TOL,nmax)
ttrap=intv(1); % Inicializo el vector de tiempo 
ytrap=y0; % Inicializo el vector del resultado
y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
for k=1:N 
    t=intv(1)+h*k; % siguiente paso 
    ttrap=[ttrap,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;
      while (midiff>TOL) && (n<=nmax) % lo calculo para la iteracion newtoniana
        y2 = y -(yant+(h/2)*(f(t,yant)+f(t+h,y)));
        % Hago la inversa
        %l = length(y0)
        jg = eye(length(y0)) - h*jfunc(t, y);
        y3 = y - (jg\y2);
        midiff=max(max(abs(y3-y)));
        y = y3;
        n=n+1;
        e=e+1;
      end
    yant = y;
    ytrap=[ytrap,y]; 
    ev=[ev,e];
end
end