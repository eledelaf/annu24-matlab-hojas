function [N_vect,Ev_vect,error_vect]=fcomparerrorfix(met,f,intv,y0,N,fexact,M,TOL,nmax)
N0=N;
error_vect=0;
N_vect=N;
Ev_vect=0;
for k=1:M
    [tvals,yvals,ev]=met(f,intv,y0,N,TOL,nmax);
    solexact1=fexact(tvals);
    error1=max(max(abs(solexact1-yvals)));
    error_vect=[error_vect,error1];
    Ev_vect=[Ev_vect,ev];
    N=N0*(2^(k));
    N_vect=[N_vect,N];
end
end