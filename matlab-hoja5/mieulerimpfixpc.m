function [teuler,yeuler,ev]=mieulerimpfixpc(f,intv,y0,N,TOL,nmax) 
disp('H4: file: mieulerimpfixpc Elena')
% Predictor Corrector
teuler=intv(1); % Inicializo el vector de tiempo 
yeuler=y0; % Inicializo el vector del resultado
%y=y0; % Dato inicial 
yant = y0;
ev=0; % Contador de evaluaciones 
h=(intv(2)-intv(1))/N; % Saco h
t = intv(1);
for k=1:N 
    y = yant + h*f(t,yant);%Predictor corrector 
    t=intv(1) + h*k; % siguiente paso 
    teuler=[teuler,t]; % Añado el tiempo al vector 
    e=0;
    n=1; % contador de veces que hace el bucle while
    midiff=1;

    while (midiff>TOL) && (n<=nmax)
        y1=yant+(h/2)*(f(t,yant)+f(t+h,y)); 
        midiff=max(max(abs(y1-y)));
        y=y1;
        n=n+1;
        e=e+1;
    end
    yant = y;
    yeuler=[yeuler,y]; 
    ev=[ev,e];
end
end