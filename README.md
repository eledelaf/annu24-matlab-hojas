
<div id="outline-container-orgdbb08a3" class="outline-2">
<h2 id="orgdbb08a3"><span class="section-number-2">1.</span> Prácticas de Matlab (Distribución temporal de las hojas de Matlab)</h2>
<div class="outline-text-2" id="text-1">
<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="all">


<colgroup>
<col  class="org-right" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-right">Hoja</th>
<th scope="col" class="org-left">Contenido</th>
<th scope="col" class="org-left">Publicación</th>
<th scope="col" class="org-left">Objetivo</th>
<th scope="col" class="org-left">Terminación</th>
</tr>

<tr>
<th scope="col" class="org-right">&#xa0;</th>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-left">aproximadamente</th>
<th scope="col" class="org-left">&#xa0;</th>
<th scope="col" class="org-left">recomendada</th>
</tr>
</thead>
<tbody>
<tr>
<td class="org-right">1</td>
<td class="org-left">Ode45</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">23.01.2024</span></span></td>
<td class="org-left">aprender ode45, sprintf, title</td>
<td class="org-left">opcional</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-right">2</td>
<td class="org-left">Bucles simples</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">23.01.2024</span></span></td>
<td class="org-left">aprender a usar bucles sin índices</td>
<td class="org-left">opcional</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-right">3</td>
<td class="org-left">Euler/RK</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">29.01.2024</span></span></td>
<td class="org-left">Implementar métodos monopaso</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">12.02.2024</span></span></td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">explícitos</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-right">4</td>
<td class="org-left">Diagrama de eficiencia</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">06.02.2024</span></span></td>
<td class="org-left">Construir diagramas para calcular</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">20.02.2024</span></span></td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">el orden de un método</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-right">5</td>
<td class="org-left">Métodos implícitos</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">20.02.2024</span></span></td>
<td class="org-left">Implementar métodos monopaso</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">20.03.2024</span></span></td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">por Newton y punto fijo</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">implícitos mediante</td>
<td class="org-left">&#xa0;</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">dos esquemas: punto fijo y Newton;</td>
<td class="org-left">&#xa0;</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">diagramas de</td>
<td class="org-left">&#xa0;</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">eficiencia</td>
<td class="org-left">&#xa0;</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-right">6</td>
<td class="org-left">Métodos Multipaso</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">20.03.2024</span></span></td>
<td class="org-left">Implementar un método multipaso</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">27.03.2024</span></span></td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">implícito mediante</td>
<td class="org-left">&#xa0;</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">Newton</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-right">7</td>
<td class="org-left">Adaptativos</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">27.03.2024</span></span></td>
<td class="org-left">Implementar métodos monopaso con</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">17.04.2024</span></span></td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">paso adaptativo</td>
<td class="org-left">&#xa0;</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">usando h<sub>opt</sub> y pares encajados</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-right">8</td>
<td class="org-left">Locus</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">10.04.2024</span></span></td>
<td class="org-left">usando cálculo simbólico, pintar</td>
<td class="org-left">opcional</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">regiones de</td>
<td class="org-left">&#xa0;</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">estabilidad</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-right">9</td>
<td class="org-left">Disparo</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">17.04.2024</span></span></td>
<td class="org-left">usando RK, implementar el disparo</td>
<td class="org-left"><span class="timestamp-wrapper"><span class="timestamp">28.04.2024</span></span></td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">lineal para</td>
<td class="org-left">&#xa0;</td>
</tr>

<tr>
<td class="org-right">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">&#xa0;</td>
<td class="org-left">condiciones de Dirichlet y Neuman</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
</table>
</div>
</div>
