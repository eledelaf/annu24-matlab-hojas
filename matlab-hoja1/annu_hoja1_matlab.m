%% Practicas de Matlab
%% Rutinas de Matlab y la resolución de EDO
%% Hoja 1 % UB:29.01.2024:15:12: Confirmado, prueba notificacion, Usuario+Email, mal
% *Nombre:* Elena 
% 
% *Apellido:* de la Fuente
% 
% *DNI:* 23821952R
%% 
% %% Práctica 1 (EDO de corazón)
% Considera el siguiente PVI
% 
% $$    \begin{array}{ccc}    \frac{dx_1}{dt} & = & x_2                 \\    
% \frac{dx_2}{dt} & = & -16x_1 + 4 \sin(2t) \\    x_1(0)          & = & 0                  
% \\    x_2(0)          & = & 2    \end{array}$$
% 
% en el intervalo, $[0,2 \pi]$. Ahora intenta resolverla numéricamente usando 
% el comando *ode45* de Matlab y pinta la solución
% 
% *Solución*
disp('Eso es el codigo de UB')
f = @(t, y) [y(2); -16*y(1)+4*sin(2*t)];
met='ode45';
intv=[0 2*pi];
x0=[0;2];
[t,y] = ode45(f, intv, x0);

i=1;
figure(i)
set(gca,'FontSize',16);
plot(t, y(:, 1), 'go-', t, y(:, 2), 'r+-')
s=sprintf('Ecuacion de Corazon,\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0);
title(s)
grid on

i=2;
figure(i)
set(gca,'FontSize',16);
hold on 
plot(y(:,1),y(:,2),'r-+')
hold off
grid on
s=sprintf('Diagrama de fase (Corazon),\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0);
title(s)
%% Práctica 2 (Sistema depredador-presa)
% Sea $x(t)$ la población de la especie presa, y $y(t)$ la de la especie depredadora,  
% $x'(t):=\frac{dx}{dt}$ y $y'(t):=\frac{dy}{dt}$ representarán los cambios de 
% las poblaciones con respecto del tiempo y vienen regidos por un sistema de 
%% 
% * un sistema de _Lotka-Volterra_
%% 
% $$        \begin{array}{ccl}          x^{\prime} & = & Ax - Byx \qquad A,B\in 
% {\mathord{\mathbb R}}^{+}   \\[0.2cm]          y^{\prime} & = & -Cy + Dxy \qquad   
% C,D\in {\mathord{\mathbb R}}^{+} \\[0.2cm]          x(0) & = & \alpha_1                                            
% \\[0.2cm]          y(0) & = & \alpha_2        \end{array}$$
%% 
% * o un o por un sistema de _Lotka-Volterra_ con un depredador externo (un 
% pescador)
%% 
% $$        \begin{array}{ccl}         x^{\prime} & = & A x - B yx -E x \qquad 
% A,B,E\in {\mathord{\mathbb R}}^{+} \\[0.2cm]         y^{\prime} & = & -C y + 
% Dxy - E y \qquad C,D\in {\mathord{\mathbb R}}^{+}  \\[0.2cm]          x(0)      
% & = & \alpha_1                                                  \\[0.2cm]          
% y(0)      & = & \alpha_2        \end{array}$$
%% 
% * ¿Existe algún punto de equilibrio para este modelo de población? En ese 
% caso, ¿ para qué valores de $x_1,x_2$ el punto de equilibrio es estable? 
%% 
% *Solución:*
%% 
% * Resuelve este sistema para $0\leq t\leq 100$, suponiendo que las constantes  
% $A,B,C,D$ describen la interacción entre las especies y su propio crecimiento 
% y decrecimiento: $A=0.4$, $B=0.01$, $C=0.3$ y $D=0.005$). Considera los datos 
% iniciales $[50;\, 30]$. Dibuja las componentes de solución frente el tiempo 
% en una ventana, en otra, el diagrama de fase.
%% 
% % 
% % 
% *Solución:*
disp('H1: Ej2 Elena de la Fuente')

% Sistema Lotka-Volterra
f = @(t, y) [0.4*y(1)-0.01*y(1)*y(2);
            -0.3*y(2)+0.005*y(1)*y(2)];
met='ode45';
intv=[0 100];
x0=[50;30];
[t,y] = ode45(f, intv, x0);

%Plot 1
i=3;
figure(i)
set(gca,'FontSize',16);
plot(t, y(:, 1), 'go-', t, y(:, 2), 'r+-')
s=sprintf('Ecuacion de Corazon,\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0);
title(s)
grid on

%Plot 2
i=4;
figure(i)
set(gca,'FontSize',16);
hold on 
plot(y(:,1),y(:,2),'r-+')
hold off
grid on
s=sprintf('Diagrama de fase (Corazon),\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0);
title(s)

%% 
% * En el caso del sistema con depredador externo $E=0.04$ ¿Qué tipo de trayectorias 
% describen? Dibuja una gráfica de la solución de cada problema. ¿Qué es su conclusión?
%% 
% % 
% *Solución* 
% 
% 
% Sistema Lotka-Volterra depredador externo
f = @(t, y) [0.4*y(1)-0.01*y(1)*y(2)-0.04*y(1);
            -0.3*y(2)+0.005*y(1)*y(2)-0.04*y(2)];
met='ode45';
intv=[0 100];
x0=[50;30];
[t,y] = ode45(f, intv, x0);

%Plot 1
i=5;
figure(i)
set(gca,'FontSize',16);
plot(t, y(:, 1), 'go-', t, y(:, 2), 'r+-')
s=sprintf('Ecuacion de Corazon,\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0);
title(s)
grid on

%Plot 2
i=6;
figure(i)
set(gca,'FontSize',16);
hold on 
plot(y(:,1),y(:,2),'r-+')
hold off
grid on
s=sprintf('Diagrama de fase (Corazon),\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0);
title(s)

%% Práctica 3 (Ecuación del péndulo)
% Estudiar la ecuación
% 
% $$\begin{cases}m L \theta '' (t) + 2 L \beta \theta' (t) + mg (\theta(t)) 
% = F & t \in [0,T] \\\theta(0) = \theta_0, \\\theta' (0) = w_0.\end{cases}$$
% 
% Se consideran los valores $m = 1 [kg] , g = 9.8 [m/s^2], T = 10.$ Estudiar, 
% para $F= 0, L = 1 [m]$, los diferentes comportamientos del sistema con $\beta  
% = 0, 0.25, 0.5$ partiendo del dato inicial $\theta_0 = 0, w_0 = 0.5$. Representar 
% tanto $(t, \theta(t))$ como el diagrama de fase $(\theta, \dot \theta)$.
% 
% Utilizar paso de tiempo $h = 0.01, 0.0001$¿es fidedigno el resultado con respecto 
% a las soluciones explícitas conocidas? 
% 
% *Solución:* 
disp('H1: Ej3 Elena de la Fuente')
% 
% $$\dot{\mathbf q} = \mathbf f(\mathbf q) = \left (q_2,\  \frac 1 {mL} (F - 
% 2L \beta q_2 - mgq_1) \right)$$
% 
% 
%% 
% *Solución Diagrama de Fase:*
b = 0;
h = 0.01;
tspan = 0:h:10;
x0 = [0; 0.5]; % Condiciones iniciales

[t, x] = ode45(@(t, x) odefun(t, x, b), tspan, x0);

i=7;
figure(i)
plot(t, x(:,1),'b');
title(sprintf('\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0));
xlabel('Tiempo t');
ylabel('x(t)');
hold on 

b=0.25
[t, x] = ode45(@(t, x) odefun(t, x, b), tspan, x0);
plot(t, x(:,1),'g');

b=0.5
[t, x] = ode45(@(t, x) odefun(t, x, b), tspan, x0);
plot(t, x(:,1),'r');
legend('b = 0', 'b = 0.25', 'b = 0.5');
grid on 
hold off
%%
b = 0;
h = 0.01;
tspan = 0:h:10;
x0 = [0; 0.5]; % Condiciones iniciales

[t, x] = ode45(@(t, x) odefun(t, x, b), tspan, x0);

i=8;
figure(i)
plot(x(:,1),x(:,2));
title(sprintf('\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0));
xlabel('Tiempo t');
ylabel('x(t)');
hold on 

b=0.25
[t, x] = ode45(@(t, x) odefun(t, x, b), tspan, x0);
plot(x(:,1),x(:,2),'g');

b=0.5
[t, x] = ode45(@(t, x) odefun(t, x, b), tspan, x0);
plot(x(:,1),x(:,2),'r');
legend('b = 0', 'b = 0.25', 'b = 0.5');
grid on 
hold off


%% Práctica 4: (Diagramas de fase: ecuación del péndulo *Comparar, considerando* $F = 0$, con el problema linealizado)
% Estudiar la ecuación
% 
% $$\begin{cases}m L \theta '' (t) + 2 L \beta \theta' (t) + mg \sin (\theta(t)) 
% = F & t \in [0,T] \\\theta(0) = \theta_0, \\\theta' (0) = w_0.\end{cases}$$
% 
% Se consideran los valores $m = 1 [kg] , g = 9.8 [m/s^2], T = 10.$
% 
% Comparad la ecuacion arriba,* considerando* $F = 0$, con el problema linealizado
% 
% $$\begin{cases}m L \theta '' (t) + 2 L \beta \theta' (t) + mg \theta(t) =0 
% & t \in [0,T] \\\theta(0) = \theta_0, \\\theta' (0) = w_0.\end{cases}$$
% 
% *Solucion con diferentes figures*
% 
% % 
% % 
% solución con simple plots
% Ecuación del péndulo con sin(theta)
beta = 0.1;
L = 1;
intv = [0,10];
y0 = [3.0416,0];
h = 0.01;
%tspan = intv(1):h:intv(2);
tspan = [intv(1), intv(2)];

f1 = @(t, y) [y(2); -2*beta*y(2) - (9.8/L)*sin(y(1))]; % Ecuación con seno(heta)
f2 = @(t, y) [y(2); -2*beta*y(2) - (9.8/L)*y(1)]; % Ecuacuación sin seno(theta)

[t1, y1] = ode45(f1, tspan, y0);
[t2, y2] = ode45(f2, tspan, y0);

figure;
plot(t1, y1(:,1), 'b-');
hold on;
plot(t2,y2(:,1), 'r--');
xlabel('Tiempo (s)');
ylabel('Ángulo \theta (rad)');
title('Comparación entre el péndulo linealizado y no linealizado');
legend('No linealizado', 'Linealizado');
grid on;


%% 
% %% solution with simple plots fase
% 
figure;
plot(y1(:,1),y1(:,2), 'b-');
hold on;
plot(y2(:,1),y2(:,2), 'r--');
xlabel('y');
ylabel('y');
title('Comparación entre el péndulo linealizado y no linealizado');
legend('No linealizado', 'Linealizado');
grid on;

%% 
% *Solucion con subplots*
clear all  

vy0 = [0.1 , 0.5, 1, 2]
for i = 1:4
    beta = 0.1;
    L = 1;
    intv = [0,10];
    y0 = [vy0(i),0];
    h = 0.01;
    %tspan = intv(1):h:intv(2);
    tspan = [intv(1), intv(2)];

    f1 = @(t, y) [y(2); -2*beta*y(2) - (9.8/L)*sin(y(1))]; % Ecuación con seno(heta)
    f2 = @(t, y) [y(2); -2*beta*y(2) - (9.8/L)*y(1)]; % Ecuacuación sin seno(theta)

    [t1, y1] = ode45(f1, tspan, y0);
    [t2, y2] = ode45(f2, tspan, y0);

    figure;
    subplot(length(vy0), 1, i);
    plot(t1, y1(:,1), 'b-', 'LineWidth', 2);
    hold on;
    plot(t2, y2(:,1), 'r--', 'LineWidth', 2);
    xlabel('Tiempo (s)');
    ylabel('\theta (rad)');
    title(['Comparación con \theta_0 = ', num2str(vy0(i))]);
    legend('No linealizado', 'Linealizado');
    grid on;
end
hold off


%% 
% % 
% solution with subplots fase
clear all 

vy0 = [0.1 , 0.5, 1, 2]
for i = 1:4
    beta = 0.1;
    L = 1;
    intv = [0,10];
    y0 = [vy0(i),0];
    h = 0.01;
    %tspan = intv(1):h:intv(2);
    tspan = [intv(1), intv(2)];

    f1 = @(t, y) [y(2); -2*beta*y(2) - (9.8/L)*sin(y(1))]; % Ecuación con seno(heta)
    f2 = @(t, y) [y(2); -2*beta*y(2) - (9.8/L)*y(1)]; % Ecuacuación sin seno(theta)

    [t1, y1] = ode45(f1, tspan, y0);
    [t2, y2] = ode45(f2, tspan, y0);

    figure;
    subplot(length(vy0), 1, i);
    plot(y1(:,1),y1(:,2), 'b-');
    hold on;
    plot(y2(:,1),y2(:,2), 'r--');
    xlabel('Tiempo (s)');
    ylabel('\theta (rad)');
    title(['Comparación con \theta_0 = ', num2str(vy0(i))]);
    legend('No linealizado', 'Linealizado');
    grid on;
end
hold off

%% Práctica 5 (Lorenz)
% Explora, usando *ode45*, el comportamiento de la solución en el intervalo  
% $0\leq t \leq 10$, para varias elecciones de los datos iniciales (por ejemplo 
% $(1,2,2)$)
% 
% $$    \frac{d}{dt}    \left(       \begin{array}{cc}        &x\\        &y\\        
% &z      \end{array}    \right)    =    \left(       \begin{array}{ccc}        
% s (y(t)  & -x(t)) &             \\        r x(t)   & -y(t)  & - x(t)z(t) \\        
% x(t)y(t) &        & - b z(t)      \end{array}    \right)$$
%% 
% * tomando $s = 10$, $b = \frac{8}{3}$. Ve aumentando desde $r=0.1$ a $r=1$ 
% con paso $0.1$ y luego a $r = 30$ con paso $1$. Observa la dinámica en los valores 
% intermedios $r=1$, $r=13.962$ y $r= 24.74$.
%% 
% % 
% *Solucion*
clear all 

sigma = 10;
b = 8/3;
r_values = [0.1:0.1:1, 1:1:30]; % r desde 0.1 a 1 con paso de 0.1, y luego de 1 a 30 con paso de 1

% Condiciones iniciales
initial_conditions = [1, 1, 2];

% Opciones para ode45 para mejorar la visualización
opts = odeset('RelTol',1e-8,'AbsTol',1e-10);

% Crear una figura
figure;
hold on;

% Resuelvo en función de r
for r = r_values
     % Definición de las ecuaciones de Lorenz
     lorenz = @(t, Y) [sigma*(Y(2) - Y(1)); Y(1)*(r - Y(3)) - Y(2); Y(1)*Y(2) - b*Y(3)];   
     % Intervalo de tiempo
     tspan = [0 40];
     % Resolver el sistema de Lorenz
     [~, Y] = ode45(lorenz, tspan, initial_conditions, opts);   
     % Dibujar
     plot3(Y(:,1), Y(:,2), Y(:,3));
end
% Etiquetas y título
xlabel('X');
ylabel('Y');
title('Comportamiento de la solución de las ecuaciones de Lorenz para varios r');
grid on;
view(2); % Vista 3D
hold off;


%% 
% %% 
% # Toma $r= 100.5$ y el dato inicial $(0,5,75)$ para ver una solución periódica. 
% Manteniendo el dato inicial mueve $r$ entre $99.524$ y  $100.795$ (p.ej. $r=  
% 99.65$) y observa el cambio de dinámica.
%% 
% *Solucion*
clear all 

sigma = 10;
b = 8/3;
%r_values = [0.1:0.1:1, 1:1:30]; % r desde 0.1 a 1 con paso de 0.1, y luego de 1 a 30 con paso de 1
r_values = [99.5:0.1:100.8]; % r desde 0.1 a 1 con paso de 0.1, y luego de 1 a 30 con paso de 1

% Condiciones iniciales
initial_conditions = [0,5,75];

% Opciones para ode45 para mejorar la visualización
opts = odeset('RelTol',1e-8,'AbsTol',1e-10);

% Crear una figura
figure;
hold on;

% Resuelvo en función de r
for r = r_values
     % Definición de las ecuaciones de Lorenz
     lorenz = @(t, Y) [sigma*(Y(2) - Y(1)); Y(1)*(r - Y(3)) - Y(2); Y(1)*Y(2) - b*Y(3)];   
     % Intervalo de tiempo
     tspan = [0 40];
     % Resolver el sistema de Lorenz
     [t, Y] = ode45(lorenz, tspan, initial_conditions, opts);   
     % Dibujar
     plot3(Y(:,1), Y(:,2), Y(:,3));
end
% Etiquetas y título
xlabel('X');
ylabel('Y');
title('Comportamiento de la solución de las ecuaciones de Lorenz para varios r');
grid on;
view(2); % Vista 3D
hold off;


%% Práctica 6 (Problema de tres cuerpos)
% Consideramos el problema de tres cuerpos restringidos en la siguiente manera: 
% dos cuerpos con masas $1-\mu$ y $\mu$ se mueven en círculos en un plano mientras 
% que un tercer cuerpo con una masa que es despreciable en comparación con los 
% otras dos masas se mueve en el mismo plano. Las ecuaciones para el tercer cuerpo 
% son:
% 
% $$    \begin{array}{ccl}      x'' &=& x + 2 y' - \left( 1-m\right)\frac{x+m 
% }{D1}      - m \frac{x - (1-m)}{D2}\\      y'' &=& y - 2       x'      - \left( 
% 1-m      \right)\frac{y }{D1} - m \frac{y }{D2}\\      D1 &=&      \sqrt[3]{\left( 
% \left( x+m \right)^2+ y^2 \right)^{2}}      \\      D2 &=&      \sqrt[3]{\left(\left(x-(1-m) 
% \right)^2+ y^2\right)^{2}}\\      &m&=0.012277471     \end{array}$$
%% 
% # Transforma el sistema en un sistema equivalente de primer orden.
% # Resuelve el sistema para los datos iniciales 
%% 
% $$\begin{array}{ccc} x(0)     & = & 0.994                            \\ x'(0)    
% & = & 0                                \\ y(0)     & = & 0                                
% \\y'(0)     & = & -2.00158510637908252240537862224 \\T_{period} & = & 17.0652165015796255 
% \qquad\mbox{El periodo de la solución}\end{array} $$
% 
% con el método de *ode45* y pinta $x(t)$ frente a $y(t)$.
% 
% % 
% *Solucion*


% Parámetro m
m = 0.012277471;

% Definición de la función utilizando la notación correcta de argumentos
f = @(t, u) [u(2); 
             u(1) + 2*u(4) - (1 - m) * (u(1) + m) / (((u(1) + m)^2 + u(3)^2)^(3/2)) - m * (u(1) - (1 - m)) / (((u(1) - (1 - m))^2 + u(3)^2)^(3/2));
             u(4); 
             u(3) - 2*u(2) - (1 - m) * u(3) / (((u(1) + m)^2 + u(3)^2)^(3/2)) - m * u(3) / (((u(1) - (1 - m))^2 + u(3)^2)^(3/2))];

% Condiciones iniciales
u0 = [0.994, 0, 0, -2.001];

% Intervalo de tiempo
tspan = [0, 17.0652165015796255]; % Periodo de la solución

% Opciones para ode45 para mejorar la visualización
opts = odeset('RelTol',1e-8,'AbsTol',1e-10);

% Resolver el sistema usando ode45
[t, U] = ode45(f, tspan, u0, opts);

% Graficar x(t) vs y(t)
figure;
plot(U(:,1), U(:,3));
xlabel('x(t)');
ylabel('y(t)');
title('Trayectoria del tercer cuerpo en el problema de tres cuerpos');
grid on;


%%%%
% Funciones auxiliares
% 
% function dxdt = odefun(t, x, b)
%     dxdt = [x(2); -2*b*x(2) - 9.8*x(1)];
% end
