function dxdt = odefun(t, x, b)
    dxdt = [x(2); -2*b*x(2) - 9.8*x(1)];
end