function [t1,u1]=midisplin(p,q,r,intv,a,b,N,c1,c2);

% C1 = 0 la condición 1 de Dirichlet x(t0) = a
% C1 = 1 la condición 1 de Neuman x'(t0) = a

% C2 = 0 la condición 2 de Dirichlet x(T) = b
% C2 = 1 la condición 2 de Neuman x'(T) = b
