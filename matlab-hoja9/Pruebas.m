import miDD.*

disp('H7: cod UB P1')
p=@(t)[0*t];
q=@(t)[4+0*t];
r=@(t)[-4*t];
intv=[0 1];
a=-5;
b=2;
c1=0;
c2=0;
N=10;

[t,y,y1,y2] = miDD(p,q,r,intv,a,b,N);

figure(1)
grid on
hold on
plot(t,y(:,1),'r-*'), 
plot(t,y(:,2),'b-*'), 
legend('solucion','derivada')
grid on
title('$x^{\prime\prime}(t) =(1-\sin(t))x^{\prime}(t) + \cos(t) x(t) + \sin(t) \quad 0\leq t \leq 1 \quad x^{\prime}(0)=-2, \quad x^{\prime}(10)=-1$','Interpreter','latex')
hold off

%%
import miNN.*
p=@(t)[1-sin(t)];
q=@(t)[cos(t)];
r=@(t)[sin(t)];
intv=[0 10];
a=-2;
b=-1;
c1=1;
c2=1;
N=10;

[t,y,y1,y2] = miNN(p,q,r,intv,a,b,N)

figure(2)
grid on
hold on
plot(t,y(:,1),'r-*'), 
plot(t,y(:,2),'b-*'), 
legend('solucion','derivada')
grid on
title('$x^{\prime\prime}(t) =(1-\sin(t))x^{\prime}(t) + \cos(t) x(t) + \sin(t) \quad 0\leq t \leq 1 \quad x^{\prime}(0)=-2, \quad x^{\prime}(10)=-1$','Interpreter','latex')
hold off

%%
import miND.*

p=@(t)[0*t];
q=@(t)[cos(t)];
r=@(t)[t];
intv=[0 10];
a=-2;
b=-1;
c1=1;
c2=0;
N=10;

[t,y,y1,y2] = miND(p,q,r,intv,a,b,N);
figure(1)
grid on
hold on
plot(t,y(:,1),'r-*'), 
plot(t,y(:,2),'b-*'), 
legend('solucion','derivada')
grid on

title('$x^{\prime\prime}(t) = \cos(t) x(t)+t, \quad 0\leq t \leq    10 \quad    x^{\prime}(0)=-2, \quad x(10)=-1$','Interpreter','latex')

hold off

%%
import miDN.*
p=@(t)[3+0*t];
q=@(t)[2+0*t];
r=@(t)[3*cos(t)];
intv=[0 5];
a=-2;
b=1;
c1=0;
c2=1;
N=10;
[t,y,y1,y2] = miDN(p,q,r,intv,a,b,N);

figure(1)
grid on
hold on
plot(t,y(:,1),'r-*'), 
plot(t,y(:,2),'b-*'), 
legend('solucion','derivada')
grid on
title('$x^{\prime\prime}(t) =3x^{\prime}(t) +2x(t)+3\cos(t) \quad     0\leq     t \leq 5, \quad x(0)=-2, \quad x^{\prime}(5)=1$','Interpreter','latex')
hold off

