function [t,y,y1,y2] = miDN(p,q,r,intv,a,b,N)
% x(t) = x1(t)+ s * x2(t)
% x(t0) = a  x'(T) = b
 f1 = @(t, y) [y(2) ; p(t)*y(2) + q(t)*y(1) + r(t)];
 y10 = [a ; 0];
 [t,y1] = mirk4(f1,intv,y10,N);

 f2 = @(t, y)[y(2) ; p(t)*y(2) + q(t)*y(1)];
 y20 = [0 ; 1];
 [~,y2] = mirk4(f2,intv,y20,N);

if y2(end,2)== 0
    if y1(end,2) == b
        disp('Hay inf soluciones')
        y = NaN
    else 
        disp('No hay solucion')
        y = NaN
    end
else
for i = 1:N+1
    y(i,:) = y1(i,:) + ((b-y1(end,2))/y2(end,2))* y2(i,:);
end
end
end


function[t,y] = mirk4(f,intv,y0,N)
y(1,:) = y0;
h = (intv(2) - intv(1))/N;
t = intv(1):h:intv(2);
for i = 2:N+1
    F1 = f(t(i-1), y(i-1,:));
    F2 = f(t(i-1) + h/2, y(i-1,:)+ h/2*F1.');
    F3 = f(t(i-1) + h/2, y(i-1,:)+ h/2*F2.');
    F4 = f(t(i-1) + h, y(i-1,:)+ h*F3.');
    y(i,:) = y(i-1,:) + h/6*(F1 + 2*F2 + 2*F3 + F4).';   
end
end