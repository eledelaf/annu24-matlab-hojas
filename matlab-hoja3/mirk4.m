function[t,y] = mirk4(f,intv,y0,N)
y(1,:) = y0;
t(1) = intervalo(1);
h = (intv(2) - intv(1))/N;
for i = 2:N+1
    t(i) = t(1) + (i-1)*h; 
    F1 = f(t(i-1), y(i-1,:));
    F2 = f(t(i-1) + h/2, y(i-1,:)+ h/2*F1.');
    F3 = f(t(i-1) + h/2, y(i-1,:)+ h/2*F2.');
    F4 = f(t(i-1) + h, y(i-1,:)+ h*F3.');
    y(i,:) = y(i-1,:) + h(1/6*(F1 + 2*F2 + 2*F3 + F4)).';    
end