N=100;
f = @(t, y) [y(2); -16*y(1)+4*sin(2*t)];
met = 'mieulermod';
intv = [0, 2*pi];
y0 = [0; 2];
[t, y] = mieulermod(f, intv, y0, N); 
    
figure;
set(gca, 'FontSize', 16);
hold on;
plot(y(:,1), y(:,2), 'r-+');
hold off;
grid on;
title(sprintf('Diagrama de fases, met=%s, intv=[%g %g], y0=[%g %g], N=%d', met, intv, y0, i));


%%
function[t,y] = mieulermod(f,intv,y0,N)
y(1,:) = y0;
t = zeros(N);
t(1) = intv(1);
h = (intv(2)-intv(1))/N;

for i = 2:N+1
    t(i) = t(1) + (i-1)*h
    F1 = f(t(i-1), y(i-1,:));
    y(i,:) = y(i-1,:) + h*f(t(i-1)+h/2, y(i-1,:) + h/2 *F1 ).';
end 
end