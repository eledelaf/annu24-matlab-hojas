function[t,y] = mieulermej(f,intv,y0,N,h)
y(1,:) = y0;
t(1) = intv(1);
for i = 2:N+1
    t(i) = t(1) + (i-1)*h; 
    y(i,:) = y(i-1,:) +(h/2) *(f(t(i-1), x(i-1,:)) + f(t(i),y(i-1,:)+ h*f(t(i-1), x(i-1,:)))) .';
end