function[t,y, ev] = mieuler(f,intv,y0,N)
y(1,:) = y0;
h = (intv(2)-intv(1))/N;
t = intv(1):h:intv(2);
ev = 0; 
for i = 2:N+1
    y(i,:) = y(i-1,:) + h*f(t(i-1), y(i-1,:)).';
    ev = ev + 1;
end
end