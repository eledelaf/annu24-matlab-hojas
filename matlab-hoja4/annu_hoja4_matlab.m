%% Prácticas de Matlab
%% Diagrama de eficiencia con métodos monopaso explícitos
%% Hoja 4
% *Nombre:* Elena
% 
% *Apellido:* de la Fuente 
% 
% *DNI:* 23821952R
% 
% *Email:* eledelaf@ucm.es
%% 
% %% 1. Diagrama de eficiencia
% Práctica 1 (El método de Euler explícito) 
% Consideramos el siguiente problema lineal
% 
% $$   y^{\prime}(t)=Ay(t)+B(t) \quad\mbox{para} \quad 0\leq t\leq 10,\quad  
% y(0)=(2,3)^{T},$$
% 
% $$    A=\left(\begin{array}{cc}        -2 & 1\\        1 & -2      \end{array}\right)    
% \qquad    B(t) =\left(\begin{array}{l}        2\sin(t)\\        2(\cos(t)-\sin(t)      
% \end{array}\right)$$
% 
% La solución exacta es:
% 
% $$  y=2e^{-t}\left(\begin{array}{l}      1\\      1    \end{array}\right)  
% +  \left(\begin{array}{l}      \sin(t)\\      \cos(t)    \end{array}\right)$$
% 
% Se pide lo siguiente
%% 
% # Resuelve este sistema mediante el método de _Euler explícito,_ almacena 
% el máximo en valor absoluto de la diferencia entre la solución exacta y la solución 
% numérica calculada.  *Indicación:* piensa qué norma vas a usar, dependiendo 
% del tipo de salida (vector columna o vector fila) que haya producido tu algoritmo. 
% Efectúa este cálculo para varias elecciones
% # del paso $h_j$ con $j=0,\ldots,7$ siendo $h_0=0.1$, $h_j=\frac{h_0}{2^j}$. 
% Almacena los diferentes valores de $h_i$ en un vector $h_{vect}$.
% # del número de puntos $N$ siendo $N_0=100$, $N_i=2^{i}N_0$. Almacena los 
% diferentes valores de $N_i$ en un vector $N_{vect}$.
% # número de las evaluaciones totales $Ev_i$ que realiza cada algoritmo para 
% cada valor de $h_i$. Almacena los valores en un vector $Ev_{vect}$.
% # Almacena los distintos errores en un vector de nombre  *error_euler*
%% 
% Además
%% 
% * Dibuja, en una misma ventana, en escala logarítmica, el error almacenado 
% en el apartado anterior frente al paso $h$,  $h_{vect}$ *Indicación:* usa el 
% comando |loglog| en vez del comando |plot|. No use los comandos hold on, hold 
% off
% * Repite en otra figura lo mismo pero dibujando el error frente al vector  
% $N_{vect}$ 
% * Calcula la pendiente da la recta.
% * Repite en otra figura lo mismo pero dibujando el error frente al vector  
% $Ev_{vect}$.
% * Interpreta el resultado.
%% 
% %  Práctica 2 (Euler mejorado) 
% Repite el apartado anterior con el método de Euler mejorado 
% 
% % Práctica 3 (Euler modificado)
% Repite el apartado anterior con el método de Euler modificado
% 
% % Práctica 4 (Runge-Kutta 4)
% Repite el apartado anterior con el método de Runge-Kutta de orden 4.
% 
% % 
% % 
% *OJO:*  pon siempre el diagrama de eficiencia de Euler, Euler modificado, 
% Euler mejorado y Runge Kutta 4 en una gráfica como por ejemplo:
% 
% % 
% % 
% % 
% *Solución:*
import mieuler.*
import mieulermej.*
import mieulermod.*
import mirk4.*


f= @(t,y) [-2*y(1) + y(2)+ 2*sin(t) ; 
        y(1) - 2*y(2) + 2*(cos(t)-sin(t))];

intv = [0 10];
y0 = [2 ; 3];
h = 0.1;
N = 100;


% Solución exacta
sol = @(t) [2*exp(-t) +  sin(t); 
    2*exp(-t)+cos(t)];

%Creo los vectores
h_vect = 0.1 ./ (2.^(1:7)); % Creo el vector h, h0 = 0.1, hj=h0/2^j, j = 0,...,7
N_vect = 100 .* (2.^(1:7)); % Creo el vector N, N0 = 100, Nj=N0 * 2^j, j = 0,...,7
%% Euler explicito 

% Vector donde pongo el numero de evaluciones que realiza cada algoritmo
% para cada valor de hi
Ev_vect_exp = [];% Lo hago de la misma longitud que h_vect 

%Vector para poner los errores 
error_euler_exp = [];

% Bucle para calcular errores 
for i = 1:7
    h = h_vect(i); 
    y = y0;
    vy = []; % aqui meto las f % Igual esto no hace falta
    z = y0;   
    
    for k = 1:N_vect(i)
        [t, vy,ev] = mieuler(f, intv, y0, k); 
    end
    vz  = sol(t).';
    error_euler_exp(i) = max(max(abs(vy-vz)));% Añado los errores a este vector en la posicion i  
    Ev_vect_exp(i) = ev;
end

y1 = vy(:,1);
%y2 = vy(:,end);
y2 = vy(:,2);

pen1 = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1))) % calculo de la pendiente

%% Euler modificado

% Vector donde pongo el numero de evaluciones que realiza cada algoritmo
% para cada valor de hi
Ev_vect_mod = []; % Lo hago de la misma longitud que h_vect 

%Vector para poner los errores 
error_euler_mod = [];

% Bucle para calcular errores 
for i = 1:7 
    h = h_vect(i); 
    y = y0;
    vy = []; % aqui meto las f
    z = y0;   
    
    for k = 1:N_vect(i)
        [t, vy,ev] = mieulermod(f, intv, y0, k); 
    end
    vz  = sol(t).';
    error_euler_mod(i) = max(max(abs(vy-vz)));% Añado los errores a este vector en la posicion i  
    Ev_vect_mod(i) = ev;
end

y1 = vy(:,1);
%y2 = vy(:,end);
y2 = vy(:,2);

pen2 = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1))) % calculo de la pendiente

%% Euler mej

% Vector donde pongo el numero de evaluciones que realiza cada algoritmo
% para cada valor de hi
Ev_vect_mej = []; % Lo hago de la misma longitud que h_vect 

%Vector para poner los errores 
error_euler_mej = [];

% Bucle para calcular errores 
for i = 1:7
    h = h_vect(i); 
    y = y0;
    vy = []; % aqui meto las f
    z = y0;   
    
    for k = 1:N_vect(i)
        [t, vy,ev] = mieulermej(f, intv, y0, k); 
    end
    vz  = sol(t).';
    error_euler_mej(i) = max(max(abs(vy-vz)));% Añado los errores a este vector en la posicion i  
    Ev_vect_mej(i) = ev;
end

y1 = vy(:,1);
%y2 = vy(:,end);
y2 = vy(:,2);

pen3 = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1))) % calculo de la pendiente

%% Euler RK

Ev_vect = []; % Lo hago de la misma longitud que h_vect 

%Vector para poner los errores 
error_rk = [];

% Bucle para calcular errores 
for i = 1:7
    h = h_vect(i); 
    y = y0;
    vy = []; % aqui meto las f
    z = y0;   
    
    for k = 1:N_vect(i)
        [t, vy,ev] = mirk4(f, intv, y0, k); 
    end
    vz  = sol(t).';
    error_rk(i) = max(max(abs(vy-vz)));% Añado los errores a este vector en la posicion i  
    Ev_vect(i) = ev;
end

y1 = vy(:,1);
%y2 = vy(:,end);
y2 = vy(:,2);

pen4 = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1))) % calculo de la pendiente

%%
% Error vs h
figure(1)
set(gca,'FontSize',16);
loglog(h_vect,error_euler_exp,'bo-',h_vect,error_euler_mod,'r*-',h_vect,error_euler_mej,'g+-',h_vect,error_rk,'ko-')
s=sprintf('Error maximo vs h \n problema no stiff \n intv=[0 10] y0=[2 3] \n',intv,y0);
title(s)
grid on
ylabel('Error maxmax')
xlabel('h')
legend('Metodo de Euler','Metodo de Euler Modificado','Metodo de Euler Mejorado','RK4')

%%
% Error vs N
figure(2)
set(gca,'FontSize',16);
%loglog(error_euler_exp,N_vect,'bo-',error_euler_mod,N_vect,'r*-',error_euler_mej,N_vect,'g+-',error_rk,N_vect,'ko-')
loglog(N_vect,error_euler_exp,'bo-',N_vect,error_euler_mod,'r*-',N_vect,error_euler_mej,'g+-',N_vect,error_rk,'ko-')
s=sprintf('Error maximo vs h \n problema no stiff \n intv=[0 10] y0=[2 3] \n',intv,y0);
title(s)
grid on
ylabel('Error maxmax')
xlabel('h')
legend('Metodo de Euler','Metodo de Euler Modificado','Metodo de Euler Mejorado','RK4')

%%
% Error vs Ev
figure(3)
set(gca,'FontSize',16);
loglog(Ev_vect_exp,error_euler_exp,'bo-',Ev_vect_mod,error_euler_mod,'r*-',Ev_vect_mej,error_euler_mej,'g+-',Ev_vect,error_rk,'ko-')
s=sprintf('Error maximo vs h \n problema no stiff \n intv=[0 10] y0=[2 3] \n',intv,y0);
title(s)
grid on
ylabel('Error maxmax')
xlabel('h')
legend('Metodo de Euler','Metodo de Euler Modificado','Metodo de Euler Mejorado','RK4')

%%
function[t,y,ev] = mirk4(f,intv,y0,N)
y(1,:) = y0;
h = (intv(2) - intv(1))/N;
t = intv(1):h:intv(2);
ev = 0; 
for i = 2:N+1
    F1 = f(t(i-1), y(i-1,:));
    F2 = f(t(i-1) + h/2, y(i-1,:)+ h/2*F1.');
    F3 = f(t(i-1) + h/2, y(i-1,:)+ h/2*F2.');
    F4 = f(t(i-1) + h, y(i-1,:)+ h*F3.');
    y(i,:) = y(i-1,:) + h/6*(F1 + 2*F2 + 2*F3 + F4).';   
    ev = ev + 4;
end
end

%%
function[t,y, ev] = mieuler(f,intv,y0,N)
y(1,:) = y0;
h = (intv(2)-intv(1))/N;
t = intv(1):h:intv(2);
ev = 0; 
for i = 2:N+1
    y(i,:) = y(i-1,:) + h*f(t(i-1), y(i-1,:)).';
    ev = ev + 1;
end
end
%%
% Implementación Euler mejorado explicito
function[t,y,ev] = mieulermej(f,intv,y0,N)
y(1,:) = y0;
h = (intv(2)-intv(1))/N;
t = intv(1):h:intv(2);
ev = 0;
for i = 2:N+1
    F1 = f(t(i-1), y(i-1,:));
    F2 = f(t(i), y(i-1,:) + h*F1.');
    y(i,:) = y(i-1,:) + (h/2)*(F1 + F2) .';
    ev = ev + 2;
end
end

%%
% Implementación Euler modificado
function[t,y,ev] = mieulermod(f,intv,y0,N)
y(1,:) = y0;
h = (intv(2)-intv(1))/N;
t = intv(1):h:intv(2);
ev = 0;
for i = 2:N+1
   F1 = f(t(i-1), y(i-1,:));
   F2 = f(t(i-1)+h/2, y(i-1,:) + h/2*F1.' );
   y(i,:) = y(i-1,:) + h*F2.';
   ev =  ev + 2;
end 
end
