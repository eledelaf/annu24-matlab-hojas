% Implementación Euler modificado
function[t,y,ev] = mieulermod(f,intv,y0,N)
y(1,:) = y0;
h = (intv(2)-intv(1))/N;
t = intv(1):h:intv(2);
ev = 0;
for i = 2:N+1
   F1 = f(t(i-1), y(i-1,:));
   F2 = f(t(i-1)+h/2, y(i-1,:) + h/2*F1.' );
   y(i,:) = y(i-1,:) + h*F2.';
   ev =  ev + 2;
end 
end