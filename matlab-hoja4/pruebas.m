import mieuler.*
import mieulermej.*
import mieulermod.*
import mirk4.*

f= @(t,y) [-2*y(1) + y(2)+ 2*sin(t) ; 
        y(1) - 2*y(2) + 2*(cos(t)-sin(t))];

intv = [0 10];
y0 = [2 ; 3];
h = 0.1;
N = 100;


% Solución exacta
sol = @(t) [2*exp(-t) +  sin(t); 
    2*exp(-t)+cos(t)];

%Creo los vectores
h_vect = 0.1 ./ (2.^(0:7)); % Creo el vector h, h0 = 0.1, hj=h0/2^j, j = 0,...,7
N_vect = 100 .* (2.^(0:7)); % Creo el vector N, N0 = 100, Nj=N0 * 2^j, j = 0,...,7

% Vector donde pongo el numero de evaluciones que realiza cada algoritmo
% para cada valor de hi
Ev_vect_exp = [];% Lo hago de la misma longitud que h_vect 

%Vector para poner los errores 
error_euler_exp = [];


% Bucle para calcular errores 
for i = 1:8 
    h = h_vect(i); 
    y = y0;
    vy = []; % aqui meto las f
    z = y0;   
    
    for k = 1:N_vect(i)
        [t, vy,ev] = mieuler(f, intv, y0, k); 
    end
    vz  = sol(t).';
    error_euler_exp(i) = max(max(abs(vy-vz)));% Añado los errores a este vector en la posicion i  
    Ev_vect_exp(i) = ev;
end

y1 = vy(:,1);
%y2 = vy(:,end);
y2 = vy(:,2);

pen1 = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1))) % calculo de la pendiente

%% Euler modificado

% Vector donde pongo el numero de evaluciones que realiza cada algoritmo
% para cada valor de hi
Ev_vect_mod = []; % Lo hago de la misma longitud que h_vect 

%Vector para poner los errores 
error_euler_mod = [];

% Bucle para calcular errores 
for i = 1:8 
    h = h_vect(i); 
    y = y0;
    vy = []; % aqui meto las f
    z = y0;   
    
    for k = 1:N_vect(i)
        [t, vy,ev] = mieulermod(f, intv, y0, k); 
    end
    vz  = sol(t).';
    error_euler_mod(i) = max(max(abs(vy-vz)));% Añado los errores a este vector en la posicion i  
    Ev_vect_mod(i) = ev;
end

y1 = vy(:,1);
%y2 = vy(:,end);
y2 = vy(:,2);

pen2 = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1))) % calculo de la pendiente

%% Euler mej

% Vector donde pongo el numero de evaluciones que realiza cada algoritmo
% para cada valor de hi
Ev_vect_mej = []; % Lo hago de la misma longitud que h_vect 

%Vector para poner los errores 
error_euler_mej = [];

% Bucle para calcular errores 
for i = 1:8 
    h = h_vect(i); 
    y = y0;
    vy = []; % aqui meto las f
    z = y0;   
    
    for k = 1:N_vect(i)
        [t, vy,ev] = mieulermej(f, intv, y0, k); 
    end
    vz  = sol(t).';
    error_euler_mej(i) = max(max(abs(vy-vz)));% Añado los errores a este vector en la posicion i  
    Ev_vect_mej(i) = ev;
end

y1 = vy(:,1);
%y2 = vy(:,end);
y2 = vy(:,2);

pen3 = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1))) % calculo de la pendiente

%% Euler RK

Ev_vect = []; % Lo hago de la misma longitud que h_vect 

%Vector para poner los errores 
error_rk = [];

% Bucle para calcular errores 
for i = 1:8 
    h = h_vect(i); 
    y = y0;
    vy = []; % aqui meto las f
    z = y0;   
    
    for k = 1:N_vect(i)
        [t, vy,ev] = mirk4(f, intv, y0, k); 
    end
    vz  = sol(t).';
    error_rk(i) = max(max(abs(vy-vz)));% Añado los errores a este vector en la posicion i  
    Ev_vect(i) = ev;
end

y1 = vy(:,1);
%y2 = vy(:,end);
y2 = vy(:,2);

pen4 = (log(y2(2))-log(y1(2)))/(log(y2(1))-log(y1(1))) % calculo de la pendiente

%%
% Error vs h
figure(1)
set(gca,'FontSize',16);
loglog(h_vect,error_euler_exp,'bo-',h_vect,error_euler_mod,'r*-',h_vect,error_euler_mej,'g+-',h_vect,error_rk,'ko-')
s=sprintf('Error maximo vs h \n problema no stiff \n intv=[0 10] y0=[2 3] \n',intv,y0);
title(s)
grid on
ylabel('Error maxmax')
xlabel('h')
legend('Metodo de Euler','Metodo de Euler Modificado','Metodo de Euler Mejorado','RK4')

%%
% Error vs N
figure(2)
set(gca,'FontSize',16);
%loglog(error_euler_exp,N_vect,'bo-',error_euler_mod,N_vect,'r*-',error_euler_mej,N_vect,'g+-',error_rk,N_vect,'ko-')
loglog(N_vect,error_euler_exp,'bo-',N_vect,error_euler_mod,'r*-',N_vect,error_euler_mej,'g+-',N_vect,error_rk,'ko-')
s=sprintf('Error maximo vs h \n problema no stiff \n intv=[0 10] y0=[2 3] \n',intv,y0);
title(s)
grid on
ylabel('Error maxmax')
xlabel('h')
legend('Metodo de Euler','Metodo de Euler Modificado','Metodo de Euler Mejorado','RK4')

%%
% Error vs Ev
figure(3)
set(gca,'FontSize',16);
loglog(Ev_vect_exp,error_euler_exp,'bo-',Ev_vect_mod,error_euler_mod,'r*-',Ev_vect_mej,error_euler_mej,'g+-',Ev_vect,error_rk,'ko-')
s=sprintf('Error maximo vs h \n problema no stiff \n intv=[0 10] y0=[2 3] \n',intv,y0);
title(s)
grid on
ylabel('Error maxmax')
xlabel('h')
legend('Metodo de Euler','Metodo de Euler Modificado','Metodo de Euler Mejorado','RK4')


