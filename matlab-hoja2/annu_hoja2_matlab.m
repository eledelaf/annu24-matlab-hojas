%% Prácticas de Matlab
%% Bucles simples
%% Hoja 2
% *Nombre: Elena*
% 
% *Apellido: de la Fuente *
% 
% *DNI: 23821952R*

%% Sucesiones escalares
% % 
% Consideramos las sucesiones
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} + h x_{n}^{2}\\  t_{n+1}&=& t_{n} 
% + h   \end{array}$$
% Práctica 1 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Usad índices para realizar 
% el bucle. Datos $x(1)=1$  $t(1)=1$  $h=0.1$ $N=10$ y $N=100$
% 
% *Solución:*

%% Varaibles comunes 
h = 0.1;
N10 = 10;
N100 = 100;

%% Inicialización de variables 
x(1) = 1;
t(1) = 1;

for i = 1:N10
    x(i+1) = x(i)+h*(x(i))^2;
    t(i+1) = t(i)+h;
end


figure(1)
set(gca,'FontSize',16);
plot(t, x, 'go-')
s=sprintf('Evolución de x en N=10');
title(s)
grid on

%%
%Inicialización de variables 
x1(1) = 1;
t1(1) = 1;

for i = 1:N100
    x1(i+1) = x1(i)+h*(x1(i))^2;
    t1(i+1) = t1(i)+h;
end

figure(2)
set(gca,'FontSize',16);
plot(t1, x1, 'go-')
s=sprintf('Evolución de x en N=100');
title(s)
grid on

%% 
% Práctica 2 (Script: Bucle sin usar índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1$ 
% $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*

int = 1:N10;
x2 = [1];
t2 = [1];
while length(x2) < N10
    y2 = x2(end); % Tomo el último elemento de x
    x2 = [x2,y2+h*y2^2]; % concateno vector x con el siguiente valor de x

    s2 = t2(end);
    t2 = [t2, s2+h] ;
end

figure(3)
plot(x2)
hold on 
plot(t2)
grid on
hold off

%% 
int = 1:N100;
x3 = [1];
t3 = [1];
while length(x3) < N100
    y3 = x3(end); % Tomo el último elemento de x
    x3 = [x3,y3+h*y3^2]; % concateno vector x con el siguiente valor de x
    s3 = t3(end);
    t3 = [t3, s3+h] ;
end

figure(4)
plot(x3)
hold on 
plot(t3)
grid on
hold off

%% Sucesiones de varias componentes
% Consideramos las sucesiones 
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} - h y_{n}\\  y_{n+1}&=& y_{n} + h 
% x_{n}\\  t_{n+1}&=& t_{n} + h   \end{array}$$
% Práctica 3 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1)=1,$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*

%Inicializar variables
x4(1) = 1;
t4(1) = 1;
y4(1) = 1;
for i = 1:N10
    x4(i+1) = x4(i) - h * x4(i);
    y4(i+1) = y4(i) + h * x4(i);
    t4(i+1) = t4(i) + h;
end
figure(5)
plot(t4, x4, 'b*-')
hold on 
plot(t4,y4,'g*-')
title('Comparativa entre x e y, para N = 10')
grid on
hold off


%%
%Inicializar variables
x5(1) = 1;
t5(1) = 1;
y5(1) = 1;

for i = 1:N100
    x5(i+1) = x5(i) - h * x5(i);
    y5(i+1) = y5(i) + h * x5(i);
    t5(i+1) = t5(i) + h;
end

figure(5)
plot(t5,x5,'b*-')
hold on 
plot(t5,y5,'g*-')
title('Comparativa entre x e y, para N = 100')
grid on
hold off

%% 
% Práctica 4 (Script: Bucle sin usar índices)
% Escribid las instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1)=1,$ $t(1)=1,$ $h=0.1$,$ $N=10$ y $N=100$
% 
% *Solucion:*

% Inicializar variables
int = 1:N10;
x6 = [1];
t6 = [1];
y6 = [1];

while length(x6) < N10
    a = x6(end); 
    b = y6(end);

    x6 = [x6, a-h*b]; 
    y6 = [y6, b+h*a];

    c = t6(end);
    t6 = [t6, c+h];
end

figure(7)
plot(t6, x6)
hold on 
plot(t6, y6)
grid on
title('Comparativa entre x e y, para N = 10')
hold off

%%
int = 1:N100;
x7 = [1];
t7 = [1];
y7 = [1];
while length(x7) < N100
    a = x7(end);
    b = y7(end);

    x7 = [x7, a-h*b]; 
    y7 = [y7, b+h*a];

    c = t7(end);
    t7 = [t7, c+h];
end

figure(8)
plot(t7, x7)
hold on 
plot(t7, y7)
title('Comparativa entre x e y, para N = 100')
grid on
hold off

%% Forma vectorial de las sucesiones
% Dadas las sucesiones: 
% 
% $$  \begin{array}{ccc}  x_{n+1}&=& x_{n} - h y_{n}\\  y_{n+1}&=& y_{n} + h 
% x_{n}\\  t_{n+1}&=& t_{n} + h   \end{array}$$
% 
% Escribid las dos primeras sucesiones en forma vectorial (en un papel o en 
% el propio mxl (usando el editor de ecuaciones).
% 
% *Solución:*
% Práctica 5 (Script: Bucle usando índices)
% Escribid las instrucciones de matlab (en forma de un _script)_ abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Usad índices para realizar 
% el bucle. Datos $x(1)=1,$ $y(1)=1,$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*
% % UB:02.02.2024:15:46: por que?
x8 = zeros(1,N10+1); %creo el vector de ceros
x8(1) = 1; % Pongo el 1 en el primer elemento del vector

y8 = zeros(1,N10+1);
y8(1) = 1;

t8 = zeros(1,N10);
t8(1) = 1;

for i = 2:N10+1
    x8(i+1) = x8(i)-h*x8(i);
    y8(i+1) = y8(i)+h*x8(i);
    t8(i+1) = t8(i)+h;
end

figure(9)
plot(t8, x8)
hold on 
plot(t8, y8)
grid on
hold off

%% 
% Práctica 6 (Script: Bucle sin usar índices)
% Escribid la instrucciones de matlab (en forma de un _script_) abajo que calculen 
% las dos sucesiones y pinten una frente a la otra. Evitad índices para realizar 
% el bucle. Usad la operación de concatenar vectores con vectores. Datos $x(1)=1,$ 
% $y(1),$ $t(1)=1,$ $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*

x9 = zeros(1, N10+1);
x9(1) = 1;
y9 = zeros(1, N10+1);
y9(1) = 1;
t9 = 0:h:N10*h;

for i = 2:N10+1
    x9(i) = x9(i-1)-h*x9(i-1); 
    y9(i) = y9(i-1)+h*x9(i-1);
end

figure(10);
plot(t9, x9);
hold on;
plot(t9, y9);
title('Evolución de x9 y y9 en función del tiempo');
xlabel('Tiempo (t)');
ylabel('Valores');
grid on;

%%
x10 = zeros(1, N10+1);
x10(1) = 1;
y10 = zeros(1, N10+1);
y10(1) = 1;
t10 = 0:h:N10*h;

for i = 2:N10+1
    x10(i) = x10(i-1)-h*x10(i-1); 
    y10(i) = y10(i-1)+h*x10(i-1);
end

figure(11);
plot(t10, x10);
hold on;
plot(t10, y10);
title('Evolución de x9 y y9 en función del tiempo');
xlabel('Tiempo (t)');
ylabel('Valores');
grid on;

%% Método de Euler
% Consideramos el método de Euler para el PVI:
% 
% $$  \begin{array}{cc}    \frac{dy}{dt}&=f(t,y)\\    y(t_0)&=\alpha  \end{array}$$
% 
% % 
% es decir
% 
% $$  y_{n+1}=y_n + h f(t_n,y_n).$$
% 
% Consideramos la ecuación diferencial (PVI)
% 
% $$  \begin{array}{cc}  \frac{d^2x}{dt^2}&=-x\\   x(0)&=1\\   \frac{dx(0)}{dt}&=1   
% \end{array}$$
% 
% Reescribid dicha ecuación como un sistema de ecuaciones y aplicad el método 
% de Euler. Escribid un _script _usando vuestros scripts anteriores (mejor implementar 
% sin índices) para resolver dicha EDO mediante el método de Euler. Pintad una 
% componente de la solución frente la otra.
% 
% Datos $x(1)=1,$ $\frac{dx(1)}{dt}=1,$ $t(1)=1$, $h=0.1,$ $N=10$ y $N=100$
% 
% *Solución:*

% Tengo una EDO de segundo orden, la tengo que pasar a un sistema de
% ecuaciones. Hago el cambio y1 = x, y2 = x'.
% Tengo las siguientes ecuaciones: y1' = y2 // y2' = -y1, con los valores
% iniciales y1(0) = 1 // y2(0) = 1.

f = @(t,y)[y(2); -y(1)];
x0=[1;1];

% h = 0.1;  Tamaño del paso 
% N100 = 100 Numero de pasos
intv = [0,N100*h] % Como empiezo en 0, luego llego hasta N veces el paso h

[t,y] = mieuler(f, intv, x0, N100,h);


figure(12)
set(gca,'FontSize',16);
plot(y(:,1),y(:,2),'r-+')
met = mieuler
s=sprintf('Diagrama de fase,\n met=%s,intv=[%g %g],\n x0=[%g %g]',met,intv,x0);
title(s)
grid on

%%
% Funciones auxiliares

% function[t,x] = mieuler(f,intv,x0,N,h)
% x(1,:) = x0;
% t(1) = intv(1);
% for i = 2:N+1
%     t(i) = t(1) + (i-1)*h; 
%     x(i,:) = x(i-1,:) + h*f(t(i-1), x(i-1,:)).';
% end
