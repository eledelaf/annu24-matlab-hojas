function[t,x] = mieuler(f,intv,x0,N,h)
x(1,:) = x0;
t(1) = intv(1);
for i = 2:N+1
    t(i) = t(1) + (i-1)*h; 
    x(i,:) = x(i-1,:) + h*f(t(i-1), x(i-1,:)).';
end